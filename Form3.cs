﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class Form3 : MetroFramework.Forms.MetroForm
    { 
        public Form3()
        {
            InitializeComponent();
        }
        private MySqlConnection connection;
        bool connecte = false;

        private void Form3_Load(object sender, EventArgs e)
        {
            connection = new MySqlConnection(Program.ConnectionString);
            connecte = true;


            if (connecte)
            {

                if (connection.State == ConnectionState.Closed)
                {
                    //historique des in dicateurs
                    MySqlDataAdapter DA = new MySqlDataAdapter("SELECT * FROM partiel", connection);
                    DataSet DS = new DataSet();
                    DA.Fill(DS);
                    dataGridView1.DataSource = DS.Tables[0];


                    //MessageBox.Show("connecter");

                }


                //historique des coefficients
                MySqlDataAdapter DA2 = new MySqlDataAdapter("SELECT * FROM coefficient", connection);
                DataSet DS2 = new DataSet();
                DA2.Fill(DS2);
                dataGridView2.DataSource = DS2.Tables[0];


            }
            timer1.Start();
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            labelDate.Text = DateTime.Now.ToLongDateString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu_T menu = new Menu_T();
            menu.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }
    }
}
