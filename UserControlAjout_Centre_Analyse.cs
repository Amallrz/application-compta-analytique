﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Compta_Analytique
{
    public partial class UserControlAjout_Centre_Analyse : UserControl
    {
        public static List<string> listCentre = new List<string>();
        public static List<string> listCentre_Aux = new List<string>();
        public UserControlAjout_Centre_Analyse()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void UserControlAjout_Centre_Analyse_Load(object sender, EventArgs e)
        {
            this.metroGrid1.DataSource = listCentre.Select(x => new { Centre_Principaux = x }).ToList();
            this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.metroGrid2.DataSource = listCentre_Aux.Select(x => new { Centre_Auxiliare = x }).ToList();
            this.metroGrid2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (metroRadio_centresPrincipaux.Checked)
            {
                listCentre.Add(this.metroTextBox1.Text);
                this.metroTextBox1.Text = "";
                this.metroGrid1.DataSource = listCentre.Select(x => new { Centre_Principaux = x }).ToList();
            }

            if (metroRadio_centresAuxiliare.Checked)
            {
                listCentre_Aux.Add(this.metroTextBox1.Text);
                this.metroTextBox1.Text = "";
                this.metroGrid2.DataSource = listCentre_Aux.Select(x => new { Centre_Auxiliare = x }).ToList();
            }
        }
        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.metroTextBox1.Text = sender.ToString();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {

        }

        private void metroGrid1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
             


        }
        int cell;
        string centre;
        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cell = metroGrid1.CurrentCell.RowIndex;
            this.metroTextBox1.Text = metroGrid1.Rows[cell].Cells[0].Value.ToString();
            centre = metroGrid1.Rows[cell].Cells[0].Value.ToString();
        }

        private void metroButton2_Click_1(object sender, EventArgs e)
        {
         
            if (listCentre.Contains(centre))
            {

                listCentre.RemoveAt(cell);
                listCentre.Insert(cell, this.metroTextBox1.Text);
                this.metroGrid1.DataSource = listCentre.Select(x => new { Centre_Principaux = x }).ToList();
            }
            if (listCentre_Aux.Contains(centre))
            {
                listCentre_Aux.RemoveAt(cell);
                listCentre_Aux.Insert(cell, this.metroTextBox1.Text);
                this.metroGrid2.DataSource = listCentre_Aux.Select(x => new { Centre_Auxiliare = x }).ToList();
            }
            //this.metroTextBox1.Text = "";
            
        }

        private void metroGrid2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            cell = metroGrid2.CurrentCell.RowIndex;
            this.metroTextBox1.Text = metroGrid2.Rows[cell].Cells[0].Value.ToString();
            centre = metroGrid2.Rows[cell].Cells[0].Value.ToString();
        }
    }
}
