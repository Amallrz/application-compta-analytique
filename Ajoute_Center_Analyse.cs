﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class Ajoute_Center_Analyse : MetroFramework.Forms.MetroForm
    {
        public static List<string> listCentre = new List<string>();
        public Ajoute_Center_Analyse()
        {
            InitializeComponent();
        }

        private void Ajoute_Center_Analyse_Load(object sender, EventArgs e)
        {
            this.metroGrid1.DataSource = listCentre.Select(x => new { Centre = x }).ToList();
            // this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            String connString = "server=localhost;database=dbcompta;username=root;password=";

            MySqlConnection conn = new MySqlConnection(connString);

            conn.Open();

            if (conn.State == ConnectionState.Open) MessageBox.Show("ok");
            else MessageBox.Show("non");
            /*listCentre.Add(this.metroTextBox1.Text);
            this.metroTextBox1.Text = "";
            this.metroGrid1.DataSource = listCentre.Select(x => new { Centre = x }).ToList();*/
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            Calcul_cout_UO form = new Calcul_cout_UO();
            form.Show();
            this.Hide();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            this.metroTextBox1.Text = sender.ToString();
        }
    }
}
