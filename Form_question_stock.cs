﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Compta_Analytique
{
    public partial class Form_question_stock : MetroFramework.Forms.MetroForm
    {
        public static string centre_aux;
        public static int centre_aux_répartition;
        public Form_question_stock()
        {
            InitializeComponent();
        }

        private void Form_centre_aux_Load(object sender, EventArgs e)
        {
            for(int i=0;i< UserControlAjout_Centre_Analyse.listCentre.Count;i++)
            {
                metroComboBox1.Items.Add(UserControlAjout_Centre_Analyse.listCentre.ElementAt(i));
            }
        }

        private void ultraButton1_Click(object sender, EventArgs e)
        {
            centre_aux = metroComboBox1.Text;
            centre_aux_répartition = int.Parse(metroTextBox1.Text);
            this.Hide();
        }

        private void ultraButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
