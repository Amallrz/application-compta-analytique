﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class UserControlCalcul_cout_approv : UserControl
    {
        public List<Class_des_UO> listUO = new List<Class_des_UO>();
        public static List<Class_calcul_approv> listAppro = new List<Class_calcul_approv>();
        public UserControlCalcul_cout_approv()
        {
            InitializeComponent();
            if(UserControlAjout_Centre_Analyse.listCentre_Aux.Count==0)
            this.listUO = UserControlCalcul_cout_UO.listUO;
            else
            this.listUO = UserControlCalcul_cout_UO_Aux.listUO;
        }
        private void UserControlCalcul_cout_approv_Load(object sender, EventArgs e)
        {
            
            if (listAppro.Count != 0)
            {
                metroGrid1.DataSource = null;
                metroGrid1.DataSource = listAppro;
            }
        }

        private void ajouterButton_Click(object sender, EventArgs e)
        {
            string matière = this.metroTextBox2.Text;
            double prix = 0;
            double prixUnit = double.Parse(this.metroTextBox4.Text);
            int quantité = int.Parse(this.metroTextBox3.Text);
            double autreCout = double.Parse(this.metroTextBox5.Text);
            prix = quantité * prixUnit;
            listAppro.Add(new Class_calcul_approv
            {
                Matière = matière,
                Quantité = quantité,
                Cout = prix + (prix * this.listUO.ElementAt(0).getCout()) / this.listUO.ElementAt(0).getNature() + autreCout* quantité
            });
            /*listAppro.Add(new Class_calcul_approv
            {
                Matière = "mp1",
                Cout = 100
            });
            listAppro.Add(new Class_calcul_approv
            {
                Matière = "mp2",
                Cout = 200
            });*/
            metroGrid1.DataSource = null;
            metroGrid1.DataSource = listAppro;
            this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            /*metroGrid1.Columns.Add(matière,matière);
            this.metroGrid1.Rows[0].Cells[matière].Value = "";
            this.metroGrid1.Rows[0].Cells[matière].Value = 
                (prix * this.listUO.ElementAt(0).getCout())/ this.listUO.ElementAt(0).getNature();*/

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
    }
}
