﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class UserControlCalcul_cout_revient : UserControl
    {
        public List<Class_calcul_cout_production> listCoutProduction = new List<Class_calcul_cout_production>();
        public static List<Class_calcul_cout_revient> listCoutRevient = new List<Class_calcul_cout_revient>();
        public static List<Class_résultat_analytique> listRésultat = new List<Class_résultat_analytique>();
        public List<Class_calcul_cout_complet> listCoutComplet = new List<Class_calcul_cout_complet>();
        public static String connString = "Server=localhost;Database=dbcompta;username=root;password=";
        MySqlConnection conn = new MySqlConnection(connString);
        int num_exercice;
        public UserControlCalcul_cout_revient()
        {
            InitializeComponent();
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //afficher cout de production
            for (int i = 0; i < listCoutProduction.Count; i++)
            {
                if (this.metroComboBox1.Text == listCoutProduction.ElementAt(i).NomProduit)
                {
                    this.metroTextBox2.Text = listCoutProduction.ElementAt(i).Cout_de_production.ToString();
                }


            }
        }

        private void UserControlCalcul_cout_revient_Load(object sender, EventArgs e)
        {
            if (listCoutRevient.Count != 0)
            {
                metroGrid1.DataSource = null;
                metroGrid1.DataSource = listCoutRevient;
            }
            if (listRésultat.Count != 0)
            {
                metroGrid2.DataSource = null;
                metroGrid2.DataSource = listRésultat;
            }
            this.listCoutProduction = UserControlCalcul_cout_production.listCoutProduction;
            //remplir combo
            for (int i = 0; i < listCoutProduction.Count; i++)
            {
                this.metroComboBox1.Items.Add(listCoutProduction.ElementAt(i).NomProduit);
            }
            //
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT max(id) FROM `coutcomplet`", conn);
                cmd.ExecuteNonQuery();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) // If you're expecting only one line, change this to if(reader.Read()).
                {
                    num_exercice = int.Parse(reader.GetString(0));
                }
                //MessageBox.Show("num "+num_exercice);
                conn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            listCoutRevient.Add(new Class_calcul_cout_revient
            {
                NomProduit = metroComboBox1.Text,
                CoutRevient = double.Parse(this.metroTextBox2.Text) + double.Parse(this.metroTextBox1.Text)
            });
            metroGrid1.DataSource = null;
            metroGrid1.DataSource = listCoutRevient;
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listCoutRevient.Count; i++)
            {
                if (listCoutRevient.ElementAt(i).NomProduit == metroComboBox1.Text)
                {
                    listRésultat.Add(new Class_résultat_analytique
                    {
                        NomProduit = metroComboBox1.Text,
                        Résultat = (double.Parse(this.metroTextBox3.Text) * double.Parse(this.metroTextBox4.Text)) - listCoutRevient.ElementAt(i).CoutRevient
                    });
                }
            }
            metroGrid2.DataSource = null;
            metroGrid2.DataSource = listRésultat;
            


        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        
        private void metroButton3_Click(object sender, EventArgs e)
        {
            try
            {
                // remplir l'historique
                
                 
                conn.Open();
                for (int i = 0; i < listCoutProduction.Count; i++)
                {
                    /*listCoutComplet.ElementAt(i).DateExercice = DateTime.Today.ToString("dd-MM-yyyy");
                    listCoutComplet.ElementAt(i).NomProduit = listCoutRevient.ElementAt(i).NomProduit;
                    listCoutComplet.ElementAt(i).CoutRevient = listCoutRevient.ElementAt(i).CoutRevient;
                    listCoutComplet.ElementAt(i).CoutProd = listCoutProduction.ElementAt(i).Cout_de_production;
                    listCoutComplet.ElementAt(i).Résultat= listRésultat.ElementAt(i).Résultat;*/

                    MySqlCommand cmd = new MySqlCommand("INSERT INTO `coutcomplet`(`Num_Exercice`, `Date_Exercice`, `Produit`,`Cout_revient`, `Cout_production`, `Resultat`)" +
                           " VALUES('"+ (num_exercice+1)+"/"+ DateTime.Today.ToString("dd-MM-yyyy") + "','" + DateTime.Today.ToString("dd-MM-yyyy") + "','" + listCoutRevient.ElementAt(i).NomProduit + "','" + listCoutRevient.ElementAt(i).CoutRevient
                           + "','" + listCoutProduction.ElementAt(i).Cout_de_production + "','" + listRésultat.ElementAt(i).Résultat + "')", conn);
                    cmd.ExecuteNonQuery();

                }
                conn.Close();
                MessageBox.Show("L'exercice a était bien enregistrer");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

               
            
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
