﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_Compta_Analytique.Classes
{
    public class Class_calcul_cout_production
    {
        public string NomProduit { get; set; }
        public List<double> Charge { get; set; }
        public double Cout_de_production { get; set; }
        public Class_calcul_cout_production()
        {

        }
        
    }
}
