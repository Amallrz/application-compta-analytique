﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_Compta_Analytique.Classes
{
    public class Class_calcul_cout_complet
    {
        public string NomProduit { get; set; }
        public int NumExercice { get; set; }
        public string DateExercice { get; set; }
        public double CoutRevient { get; set; }
        public double CoutProd { get; set; }
        public double Résultat { get; set; }
    }
}
