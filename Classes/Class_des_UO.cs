﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_Compta_Analytique.Classes
{
    public class Class_des_UO
    {
        private string Centre;
        private double Répartition;
        private int Nature;
        private double Quantité;
        private double Cout;
        public Class_des_UO()
        {

        }
        public Class_des_UO(string centre, int répartition, int nature, int quantité, double cout)
        {
            this.Centre = centre;
            this.Répartition = répartition;
            this.Nature = nature;
            this.Quantité = quantité;
            this.Cout = cout;
        }
        public string getCentre()
        {
            return this.Centre;
        }
        public double getRépartition()
        {
            return this.Répartition;
        }
        public int getNature()
        {
            return this.Nature;
        }
        public double getQuantité()
        {
            return this.Quantité;
        }
        public double getCout()
        {
            return this.Cout;
        }
        public void setCentre(string centre)
        {
            this.Centre = centre;
        }
        public void setRépartition(double répartition)
        {
            this.Répartition = répartition;
        }
        public void setNature(int Nature)
        {
            this.Nature = Nature;
        }
        public void setQuantité(double quantité)
        {
            this.Quantité = quantité;
        }
        public void setCout(double cout)
        {
            this.Cout = cout;
        }

    }
}
