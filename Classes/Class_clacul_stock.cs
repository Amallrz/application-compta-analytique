﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_Compta_Analytique.Classes
{
   public class Class_clacul_stock
    {
        public string Matière { get; set; }
        public double StockInitial { get; set; }
        public double Entrée { get; set; }
        public double Sortie { get; set; }
        public double StockFinal { get; set; }

    }
}
