﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application_Compta_Analytique.Classes
{
    class Class_des_UO_aux
    {
        private string Centre;
        private double Répartition;
        private double Répartition_des_centres;
        private double Total_répartition;
        private int Nature;
        private int Quantité;
        private double Cout;
        public Class_des_UO_aux()
        {

        }
        public Class_des_UO_aux(string centre, int répartition, int nature, int quantité, double cout)
        {
            this.Centre = centre;
            this.Répartition = répartition;
            this.Nature = nature;
            this.Quantité = quantité;
            this.Cout = cout;
        }
        public string getCentre()
        {
            return this.Centre;
        }
        public double getRépartition()
        {
            return this.Répartition;
        }
        public double getRépartition_des_centres()
        {
            return this.Répartition_des_centres;
        }
        public double getTotal_répartition()
        {
            return this.Total_répartition;
        }
        public int getNature()
        {
            return this.Nature;
        }
        public int getQuantité()
        {
            return this.Quantité;
        }
        public double getCout()
        {
            return this.Cout;
        }
        public void setCentre(string centre)
        {
            this.Centre = centre;
        }
        public void setRépartition(double répartition)
        {
            this.Répartition = répartition;
        }
        public void setRépartition_des_centres(double répartition_des_centres)
        {
            this.Répartition_des_centres = répartition_des_centres;
        }
        public void setTotal_répartition(double total_répartition)
        {
            this.Total_répartition = total_répartition;
        }
        public void setNature(int Nature)
        {
            this.Nature = Nature;
        }
        public void setQuantité(int quantité)
        {
            this.Quantité = quantité;
        }
        public void setCout(double cout)
        {
            this.Cout = cout;
        }
    }
}
