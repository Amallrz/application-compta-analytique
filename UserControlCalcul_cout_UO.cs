﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class UserControlCalcul_cout_UO : UserControl
    {
        public List<string> listCentre = new List<string>();
        public static List<Class_des_UO> listUO = new List<Class_des_UO>();
        public int i = 1;
        public UserControlCalcul_cout_UO()
        {
            InitializeComponent();
            this.listCentre = UserControlAjout_Centre_Analyse.listCentre;
        }

        private void UserControlCalcul_cout_UO_Load(object sender, EventArgs e)
        {
            this.listCentre = UserControlAjout_Centre_Analyse.listCentre;
            if (listUO.Count == 0)
            {
                remplir_Grid();

            }
            else
            {
                remplir_Grid();
                //ajouter les element de la liste dans le grid
                for (int i = 1; i <= listCentre.Count; i++)
                {
                    for (int j = 0; j < 4; j = j + 1)
                    {
                        if (j == 0)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getRépartition();
                        if (j == 1)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getNature();
                        if (j == 2)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getQuantité();
                        if (j == 3)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getCout();
                    }
                }

            }

        }
        public void remplir_Grid()
        {
            metroGrid1.Columns.Add("Centre d'analyse", "Centre d'analyse");
            this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            for (int i = 0; i < listCentre.Count; i++)
            {
                metroGrid1.Columns.Add(listCentre[i].ToString(), listCentre[i].ToString());
                this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }

            for (int i = 0; i < 5; i++)
            {
                this.metroGrid1.Rows.Add("");
            }
            this.metroGrid1.Rows[0].Cells[0].Value = "Répartition primaire";
            this.metroGrid1.Rows[1].Cells[0].Value = "Nature de l'UO";
            this.metroGrid1.Rows[2].Cells[0].Value = "Quantité de l'UO";
            this.metroGrid1.Rows[3].Cells[0].Value = "coût de l'UO";
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //List<Class_des_UO> list = new List<Class_des_UO>();

            for (int i = 1; i <= listCentre.Count; i++)
            {
                Class_des_UO class_des_UO = new Class_des_UO();
                double repartition = 1;
                int nature = 1;
                double quantité = 1;
                double cout = 1;
                for (int j = 0; j < 4; j = j + 1)
                {
                    if (j == 0) repartition = double.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                    if (j == 2) quantité = double.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                    if (j == 1) nature = int.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                }
                cout = repartition / quantité;
                this.metroGrid1.Rows[3].Cells[i].Value = cout;
                class_des_UO.setCentre(listCentre.ElementAt(i - 1));
                class_des_UO.setRépartition(repartition);
                class_des_UO.setNature(nature);
                class_des_UO.setQuantité(quantité);
                class_des_UO.setCout(cout);

                listUO.Add(class_des_UO);
            }
            //MessageBox.Show("le nombre d'element est " + listUO.Count);

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            try
            {
                string repartition = this.metroTextBox1.Text;
                string nature = this.metroTextBox2.Text;
                double quantité = double.Parse(this.metroTextBox3.Text);
                this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                this.metroGrid1.Rows[1].Cells[i].Value = nature;
                this.metroGrid1.Rows[2].Cells[i].Value = quantité;
                i++;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.metroTextBox1.Text = "0";
            this.metroTextBox2.Text = "0";
            this.metroTextBox3.Text = "0";

        }
        int cell;
        private void metroGrid1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*cell = metroGrid1.CurrentCell.RowIndex;
            this.metroTextBox1.Text = metroGrid1.Rows[cell].Cells[0].Value.ToString();
            centre = metroGrid1.Rows[cell].Cells[0].Value.ToString();*/
        }
    }
}
