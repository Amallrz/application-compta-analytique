﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Compta_Analytique
{
    static class Program
    {
        public const string DefaultConnectionString = "database = dbcompta; server = localhost; username = root; password = ";
        public static string ConnectionString { get; internal set; }

        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Configure();

            Application.Run(new Login());
        }


        static void Configure()
        {
            
            string commonAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            var myConfigFolder = Path.Combine(commonAppData, "myApp");
            var myConfigFile = Path.Combine(myConfigFolder, "config.txt");

            var foldrAlreadyExist = Directory.Exists(myConfigFolder);
            if (!foldrAlreadyExist)
                Directory.CreateDirectory(myConfigFolder);

            if (!File.Exists(myConfigFile))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(myConfigFile))
                {
                    sw.WriteLine(DefaultConnectionString);
                    ConnectionString = DefaultConnectionString;
                }
            }
            else
            {
                using (StreamReader sr = File.OpenText(myConfigFile))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        ConnectionString = s;
                    }
                }
            }
        }

    }
}