﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class UserControlCalcul_cout_UO_Aux : UserControl
    {
        public List<string> listCentre = new List<string>();
        public List<string> listCentre_Aux = new List<string>();
        public static List<Class_des_UO> listUO = new List<Class_des_UO>();
        public static double centre_répartition_aux;
        public static int centre_répartition;
        List<List<double>> mylist = new List<List<double>>();
        public int i = 1;
        public double x;
        public double y;
        public UserControlCalcul_cout_UO_Aux()
        {
            InitializeComponent();
        }
        private void UserControlCalcul_cout_UO_Aux_Load(object sender, EventArgs e)
        {
            this.listCentre = UserControlAjout_Centre_Analyse.listCentre;
            this.listCentre_Aux = UserControlAjout_Centre_Analyse.listCentre_Aux;
            if (listUO.Count == 0)
            {
                if (this.listCentre_Aux.Count == 1)
                {
                    this.groupBox1.Visible = true;
                    this.groupBox2.Visible = false;

                    this.Label1.Text = this.listCentre_Aux.ElementAt(0);
                    this.Label1.Font = new Font("Arial", 24, FontStyle.Bold);
                }
                if (this.listCentre_Aux.Count == 2)
                {
                    this.groupBox1.Visible = false;
                    this.groupBox2.Visible = true;

                    this.Label2.Text = this.listCentre_Aux.ElementAt(0);
                    this.Label3.Text = this.listCentre_Aux.ElementAt(1);
                    this.Label2.Font = new Font("Arial", 24, FontStyle.Bold);
                    this.Label3.Font = new Font("Arial", 24, FontStyle.Bold);
                }
                remplir_Grid_Aux();

            }
            else
            {

                remplir_Grid();
                //ajouter les element de la liste dans le grid
                for (int i = 1; i <= listCentre.Count; i++)
                {
                    for (int j = 0; j < 4; j = j + 1)
                    {
                        if (j == 0)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getRépartition();
                        if (j == 1)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getNature();
                        if (j == 2)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getQuantité();
                        if (j == 3)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i - 1).getCout();
                    }
                }

            }
        }
        public void remplir_Grid_Aux()
        {
            metroGrid1.Columns.Add("Centre d'analyse", "Centre d'analyse");
            //remplir les colonnes de la grid par les centres aux
            for (int i = 0; i < listCentre_Aux.Count; i++)
            {

                metroGrid1.Columns.Add(listCentre_Aux[i].ToString(), listCentre_Aux[i].ToString());
                this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            }
            //remplir les colonnes de la grid par les centres principaux
            for (int i = 0; i < listCentre.Count; i++)
            {

                metroGrid1.Columns.Add(listCentre[i].ToString(), listCentre[i].ToString());
                this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            }

            for (int i = 0; i < 7; i++)
            {
                this.metroGrid1.Rows.Add("");
            }
            this.metroGrid1.Rows[0].Cells[0].Value = "Répartition primaire";
            for (int i = 1; i <= listCentre_Aux.Count; i++)
            {
                this.metroGrid1.Rows[i].Cells[0].Value=listCentre_Aux[i-1].ToString();            
            }
            //this.metroGrid1.Rows[1].Cells[0].Value = "Répartition des centres auxiliaires";
            this.metroGrid1.Rows[listCentre_Aux.Count+1].Cells[0].Value = "Total répartition primaire";
            this.metroGrid1.Rows[listCentre_Aux.Count + 2].Cells[0].Value = "Nature de l'UO";
            this.metroGrid1.Rows[listCentre_Aux.Count + 3].Cells[0].Value = "Quantité de l'UO";
            this.metroGrid1.Rows[listCentre_Aux.Count + 4].Cells[0].Value = "coût de l'UO";
        }
        private void metroButton2_Click(object sender, EventArgs e)
        {
            List<double> mylist_;
            // try
            // {
            double repartition = double.Parse(this.metroTextBox_répartition.Text);
                string nature = this.metroTextBox_nature.Text;
                double quantité = double.Parse(this.metroTextBox_quantité.Text);
                if (this.listCentre_Aux.Count == 1)
                {
                    int centre_princ = int.Parse(this.metroTextBox_pourcentage.Text);
                    if (i<2)
                    {

                        centre_répartition_aux = repartition;
                        this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                        this.metroGrid1.Rows[1].Cells[i].Value = centre_princ;
                        this.metroGrid1.Rows[2].Cells[i].Value = 0;
                        this.metroGrid1.Rows[3].Cells[i].Value = 0;
                        this.metroGrid1.Rows[4].Cells[i].Value = 0;
                        this.metroGrid1.Rows[5].Cells[i].Value = 0;
                        i++;
                    }
                    else
                    {
                        this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                        this.metroGrid1.Rows[1].Cells[i].Value = centre_princ;
                        this.metroGrid1.Rows[2].Cells[i].Value = repartition + (centre_princ * centre_répartition_aux) / 100;
                        this.metroGrid1.Rows[3].Cells[i].Value = nature;
                        this.metroGrid1.Rows[4].Cells[i].Value = quantité;
                        i++;
                    }
                }
                if (this.listCentre_Aux.Count == 2)
                {
                    double centre_aux_1 = double.Parse(this.metroTextBox1.Text);
                    double centre_aux_2 = double.Parse(this.metroTextBox2.Text);
                    mylist_ = new List<double>();
                if (i < 3)
                {
                    //if (this.listCentre_Aux.ElementAt(i - 1) == this.metroGrid1.Columns[i].Name)
                    //{

                    //centre_répartition_aux = this.metroTextBox_.Text;
                    this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                    if (centre_aux_1 == 0)
                    {
                        this.metroGrid1.Rows[1].Cells[i].Value = 0;
                        this.metroGrid1.Rows[2].Cells[i].Value = centre_aux_2;

                        mylist_.Add(repartition);
                        mylist_.Add(centre_aux_2);
                    }
                    if (centre_aux_2 == 0)
                    {
                        this.metroGrid1.Rows[1].Cells[i].Value = centre_aux_1;
                        this.metroGrid1.Rows[2].Cells[i].Value = 0;

                        mylist_.Add(repartition);
                        mylist_.Add(centre_aux_1);
                    }
                    mylist.Add(mylist_);
                    
                    this.metroGrid1.Rows[3].Cells[i].Value = 0;
                    this.metroGrid1.Rows[4].Cells[i].Value = 0;
                    this.metroGrid1.Rows[5].Cells[i].Value = 0;
                    this.metroGrid1.Rows[6].Cells[i].Value = 0;
                    i++;
                    // }
                }
                else
                {
                    //calcule de x
                    x = (mylist.ElementAt(0).ElementAt(0)+ mylist.ElementAt(0).ElementAt(1)/100* mylist.ElementAt(1).ElementAt(0))/(1- mylist.ElementAt(0).ElementAt(1)/100* mylist.ElementAt(1).ElementAt(1)/100);
                    y= (mylist.ElementAt(1).ElementAt(0) + mylist.ElementAt(1).ElementAt(1) / 100 * mylist.ElementAt(0).ElementAt(0)) / (1 - mylist.ElementAt(0).ElementAt(1) / 100 * mylist.ElementAt(1).ElementAt(1) / 100);
                    //calcule de y
                    this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                    this.metroGrid1.Rows[1].Cells[i].Value = centre_aux_1;
                    this.metroGrid1.Rows[2].Cells[i].Value = centre_aux_2;
                    this.metroGrid1.Rows[3].Cells[i].Value = centre_aux_1*x/100+ centre_aux_2 * y / 100+ repartition;
                    this.metroGrid1.Rows[4].Cells[i].Value = nature;
                    this.metroGrid1.Rows[5].Cells[i].Value = quantité;
                    i++;
                }

                
               }

              
           // }
           /* catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }*/

            this.metroTextBox_répartition.Text = "";
            this.metroTextBox_pourcentage.Text = "";
            this.metroTextBox_nature.Text = "";
            this.metroTextBox_quantité.Text = "";
        }
       
        public void remplir_Grid()
        {
            metroGrid1.Columns.Add("Centre d'analyse", "Centre d'analyse");

            for (int i = 0; i < listCentre.Count; i++)
            {
                metroGrid1.Columns.Add(listCentre[i].ToString(), listCentre[i].ToString());
                this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }

            for (int i = 0; i < 5; i++)
            {
                this.metroGrid1.Rows.Add("");
            }
            this.metroGrid1.Rows[0].Cells[0].Value = "Répartition primaire";
            this.metroGrid1.Rows[1].Cells[0].Value = "Nature de l'UO";
            this.metroGrid1.Rows[2].Cells[0].Value = "Quantité de l'UO";
            this.metroGrid1.Rows[3].Cells[0].Value = "coût de l'UO";
        }
       

        private void metroButton1_Click(object sender, EventArgs e)
        {
            for (int i = listCentre_Aux.Count+1; i <= listCentre.Count+ listCentre_Aux.Count; i++)
            {
                Class_des_UO class_des_UO = new Class_des_UO();
                double total_repartition = 1;
                int nature = 1;
                double quantité = 1;
                double cout = 1;

                total_repartition = double.Parse(this.metroGrid1.Rows[listCentre_Aux.Count + 1].Cells[i].Value.ToString());
                nature = int.Parse(this.metroGrid1.Rows[listCentre_Aux.Count + 2].Cells[i].Value.ToString());
                quantité = double.Parse(this.metroGrid1.Rows[listCentre_Aux.Count + 3].Cells[i].Value.ToString());

                cout = total_repartition / quantité;
                this.metroGrid1.Rows[listCentre_Aux.Count + 4].Cells[i].Value = cout;

                class_des_UO.setCentre(this.metroGrid1.Columns[i].Name);
                class_des_UO.setRépartition(total_repartition);
                class_des_UO.setNature(nature);
                class_des_UO.setQuantité(quantité);
                class_des_UO.setCout(cout);

                listUO.Add(class_des_UO);
            }
            
        }

        private void metroTextBox_pourcentage_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBox_pourcentage_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
