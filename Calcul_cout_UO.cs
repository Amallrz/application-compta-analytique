﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class Calcul_cout_UO : MetroFramework.Forms.MetroForm
    {
        public List<string> listCentre = new List<string>();
        public static List<Class_des_UO> listUO = new List<Class_des_UO>();
        public int i = 1;

        public Calcul_cout_UO()
        {
            InitializeComponent();
            this.listCentre = Ajoute_Center_Analyse.listCentre;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.listCentre = Ajoute_Center_Analyse.listCentre;
            if (listUO.Count == 0)
            {
                remplir_Grid();

            }
            else
            {
                /*metroGrid1.Columns.Add("Centre d'analyse", "Centre d'analyse");
                metroGrid1.Columns.Add("Répartition primaire", "Répartition primaire");
                metroGrid1.Columns.Add("Nature de l'UO", "Nature de l'UO");
                metroGrid1.Columns.Add("Quantité de l'UO", "Quantité de l'UO");
                metroGrid1.Columns.Add("Cout de l'UO", "Cout de l'UO");

                // this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                for (int i = 0; i < listCentre.Count; i++)
                {
                    this.metroGrid1.Rows.Add("");
                }

                for (int i = 0; i < listCentre.Count; i++)
                {
                    this.metroGrid1.Rows[i].Cells[0].Value =listCentre[i];
                }
                //remplir metrogrid
                for (int i = 1; i <= 4; i++)
                {                  
                    for (int j = 0; j < listCentre.Count; j = j + 1)
                    {                        
                        this.metroGrid1.Rows[j].Cells[i].Value=listUO.ElementAt(j).getRépartition();
                    }
                    for (int j = 0; j < listCentre.Count; j = j + 1)
                    {
                        this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(j).getNature();
                    }
                    for (int j = 0; j < listCentre.Count; j = j + 1)
                    {
                        this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(j).getQuantité();
                    }
                    for (int j = 0; j < listCentre.Count; j = j + 1)
                    {
                        this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(j).getCout();
                    }
                }*/

                remplir_Grid();
                //ajouter les element de la liste dans le grid
                for (int i = 1; i <= listCentre.Count; i++)
                {
                    for (int j = 0; j < 4; j = j + 1)
                    {
                        if (j == 0)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i-1).getRépartition();
                        if (j == 1)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i-1).getNature();
                        if (j == 2)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i-1).getQuantité();
                        if (j == 3)
                            this.metroGrid1.Rows[j].Cells[i].Value = listUO.ElementAt(i-1).getCout();
                    }
                }

            }
            
            
        }
        public void remplir_Grid()
        {
            metroGrid1.Columns.Add("Centre d'analyse", "Centre d'analyse");

            for (int i = 0; i < listCentre.Count; i++)
            {
                metroGrid1.Columns.Add(listCentre[i].ToString(), listCentre[i].ToString());
                this.metroGrid1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }

            for (int i = 0; i < 5; i++)
            {
                this.metroGrid1.Rows.Add("");
            }
            this.metroGrid1.Rows[0].Cells[0].Value = "Répartition primaire";
            this.metroGrid1.Rows[1].Cells[0].Value = "Nature de l'UO";
            this.metroGrid1.Rows[2].Cells[0].Value = "Quantité de l'UO";
            this.metroGrid1.Rows[3].Cells[0].Value = "coût de l'UO";
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            try
            {
                string repartition = this.metroTextBox1.Text;
                string nature = this.metroTextBox2.Text;
                int quantité = int.Parse(this.metroTextBox3.Text);
                this.metroGrid1.Rows[0].Cells[i].Value = repartition;
                this.metroGrid1.Rows[1].Cells[i].Value = nature;
                this.metroGrid1.Rows[2].Cells[i].Value = quantité;
                i++;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.metroTextBox1.Text = "";
            this.metroTextBox2.Text = "";
            this.metroTextBox3.Text = "";

        }

        private void metroButton1_Click_1(object sender, EventArgs e)
        {
            this.metroTextBox1.Text = this.metroGrid1.Rows[1].Cells[0].Value.ToString();
        }

        private void metroButton1_Click_2(object sender, EventArgs e)
        {
            //List<Class_des_UO> list = new List<Class_des_UO>();
            
            for (int i = 1; i <= listCentre.Count; i++)
            {
                Class_des_UO class_des_UO = new Class_des_UO();
                double repartition=1;
                int nature = 1;
                int quantité=1;
                double cout = 1;
                for (int j = 0; j < 4; j=j+1)
                {
                    if (j == 0) repartition = double.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                    if (j == 2) quantité = int.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                    if (j == 1) nature = int.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                }
                cout = repartition / quantité;
                this.metroGrid1.Rows[3].Cells[i].Value = cout;
                class_des_UO.setCentre(listCentre.ElementAt(i-1));
                class_des_UO.setRépartition(repartition);
                class_des_UO.setNature(nature);
                class_des_UO.setQuantité(quantité);
                class_des_UO.setCout(cout);
                
                listUO.Add(class_des_UO);
            }
           //MessageBox.Show("le nombre d'element est " + listUO.Count);

        }

        private void metroLink2_Click(object sender, EventArgs e)
        {

        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Calcul_cout_approv form = new Calcul_cout_approv();
            form.Show();
            this.Hide();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            Ajoute_Center_Analyse form = new Ajoute_Center_Analyse();
            form.Show();
            this.Hide();
        }
    }
}
