﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class Form2 : MetroFramework.Forms.MetroForm
    {
        MySqlConnection con;
        bool connecte = false;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            timer1.Start();
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            labelDate.Text = DateTime.Now.ToLongDateString();
        }

        private void buttonAjouter_Click(object sender, EventArgs e)
        {
            //con = new MySqlConnection("database=courpartiel; server=localhost; username=root; password=");
            if (buttonAjouter.Text == "Ajouter")
            {
                con = new MySqlConnection(Program.ConnectionString);

                try
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                        //button1.Text = "se deconnecter";
                        connecte = true;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }


            double PI, PV, DI, DV, CAI, CAV, RI, RV, VD, VP, CEPD, VCA, VR, CECAR;
            PI = double.Parse(textBoxPrixInitial.Text);
            PV = double.Parse(textBoxPrixVariation.Text);
            DI = double.Parse(textBoxDemandeInitial.Text);
            DV = double.Parse(textBoxDemandeVariation.Text);
            CAI = double.Parse(textBoxCAInitial.Text);
            CAV = double.Parse(textBoxCAVariation.Text);
            RI = double.Parse(textBoxRInitial.Text);
            RV = double.Parse(textBoxRVariation.Text);

            //calcul de la variation de la demande
            VD = (DV - DI) / DI;

            //calcul de la variation du prix
            VP = (PV - PI) / PI;

            // calcul du coefficient d'elasticité prix/demande
            CEPD = VD / VP;

            //calcul de la variation du ca
            VCA = (CAV - CAI) / CAI;

            //calcul de la variation du résultat
            VR = (RV - RI) / RI;

            //calcul du coefficient d'elasticité CA/résultat
            CECAR = VR / VCA;

            textBoxCEPD.Text = CEPD.ToString("0.00");
            textBoxCECAR.Text = CECAR.ToString("0.00");


            if (connecte == true)
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO coefficient(coefPM,coefCAR,heure,date) VALUES(@coefPM, @coefCAR,@heure,@date)", con);
                cmd.Parameters.AddWithValue("@coefPM", Double.Parse(textBoxCEPD.Text));
                cmd.Parameters.AddWithValue("@coefCAR", Double.Parse(textBoxCECAR.Text));
                cmd.Parameters.AddWithValue("@heure", labelheureDate.Text);
                cmd.Parameters.AddWithValue("@date", labelDate.Text);

                //pour executer les parametres
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                //MessageBox.Show("parametres ajouter");

            }
            else
            {
                MessageBox.Show("vous n'etes pas connecter");
            }
        }

        private void textBoxPrixInitial_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxPrixInitial.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxPrixVariation_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxPrixVariation_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxPrixVariation.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxDemandeInitial_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxDemandeInitial.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxDemandeVariation_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxDemandeVariation.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCAInitial_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCAInitial.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCAVariation_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCAVariation.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxRInitial_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxRInitial.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxRVariation_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxRVariation.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu_T menu = new Menu_T();
            menu.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }
    }
}
