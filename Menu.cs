﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Compta_Analytique
{
    public partial class Menu_T : MetroFramework.Forms.MetroForm
    {
        public Menu_T()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            Form_Menu form = new Form_Menu();
            this.Hide();
            form.Show();
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            FormHistoriqueCoutComplet form = new FormHistoriqueCoutComplet();
            this.Hide();
            form.Show();
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            Form3 form = new Form3();
            this.Hide();
            form.Show();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            this.Hide();
            form.Show();
        }
    }
}
