﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Compta_Analytique
{
    public partial class Form_Menu : MetroFramework.Forms.MetroForm
    {
        //choix est utilisé pour est ce qu'on va appelé uo ou bien uo_aux et aussi pour rendre le formulaire de question invisible
        int choix = 0;
        public Form_Menu()
        {
            
            InitializeComponent();
        }

        private void Form_Menu_Load(object sender, EventArgs e)
        {

            UserControl_Calculatrice calculatrice = new UserControl_Calculatrice();
            calculatrice.Parent = panel2;
            //ajout_centre.Top = 100;
            //ajout_centre.Left = 150;
            calculatrice.Size = panel2.ClientSize;
            calculatrice.Dock = DockStyle.Fill;
            calculatrice.BringToFront();
            calculatrice.Show();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //DialogResult reponse = MessageBox.Show("Est ce qu'il ya des centres auxiliaires ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //if (reponse == DialogResult.Yes)
            {
                sidePanel.Height = button1.Height;
                sidePanel.Top = button1.Top;
                UserControlAjout_Centre_Analyse ajout_centre = new UserControlAjout_Centre_Analyse();
                ajout_centre.Parent = panel2;
                //ajout_centre.Top = 100;
                //ajout_centre.Left = 150;
                ajout_centre.Size = panel2.ClientSize;
                ajout_centre.Dock = DockStyle.Fill;
                ajout_centre.BringToFront();
                ajout_centre.Show();
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {

            if (UserControlAjout_Centre_Analyse.listCentre_Aux.Count == 0)
            {
                UserControlCalcul_cout_UO calcul_cout_UO = new UserControlCalcul_cout_UO();
                calcul_cout_UO.Parent = panel2;
                calcul_cout_UO.Top = 0;
                calcul_cout_UO.Left = 0;
                calcul_cout_UO.Size = panel2.ClientSize;
                //calcul_cout_UO.Dock = DockStyle.Fill;
                calcul_cout_UO.BringToFront();
                calcul_cout_UO.Show();
            }
            else
            {
                UserControlCalcul_cout_UO_Aux calcul_cout_UO_Aux = new UserControlCalcul_cout_UO_Aux();
                calcul_cout_UO_Aux.Parent = panel2;
                calcul_cout_UO_Aux.Top = 0;
                calcul_cout_UO_Aux.Left = 0;
                calcul_cout_UO_Aux.Size = panel2.ClientSize;
                //calcul_cout_UO.Dock = DockStyle.Fill;
                calcul_cout_UO_Aux.BringToFront();
                calcul_cout_UO_Aux.Show();
                // Form_centre_aux aux = new Form_centre_aux();
                //aux.Show();

            }
            sidePanel.Height = button2.Height;
            sidePanel.Top = button2.Top;
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sidePanel.Height = button3.Height;
            sidePanel.Top = button3.Top;
            UserControlCalcul_cout_approv calcul_cout_approv = new UserControlCalcul_cout_approv();
            calcul_cout_approv.Parent = panel2;
            calcul_cout_approv.Top = 0;
            calcul_cout_approv.Left = 0;
            calcul_cout_approv.Size = panel2.ClientSize;
            //calcul_cout_approv.Dock = DockStyle.Fill;
            calcul_cout_approv.BringToFront();
            calcul_cout_approv.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            sidePanel.Height = button5.Height;
            sidePanel.Top = button5.Top;
            UserControlCalcul_cout_production calcul_cout_prod = new UserControlCalcul_cout_production();
            calcul_cout_prod.Parent = panel2;
            calcul_cout_prod.Top = 0;
            calcul_cout_prod.Left = 0;
            calcul_cout_prod.Size = panel2.ClientSize;
            //calcul_cout_prod.Dock = DockStyle.Fill;
            calcul_cout_prod.BringToFront();
            calcul_cout_prod.Show();

            if (Form_calcul_stock.listStock.Count == 0)
            {
                DialogResult reponse = MessageBox.Show("Ajouter le stock ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (reponse == DialogResult.Yes)
                {
                    sidePanel.Height = button5.Height;
                    sidePanel.Top = button5.Top;
                    Form_calcul_stock calcul_stock = new Form_calcul_stock();
                    calcul_stock.Show();
                }
            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            sidePanel.Height = button4.Height;
            sidePanel.Top = button4.Top;
            UserControlCalcul_cout_revient calcul_cout_revient = new UserControlCalcul_cout_revient();
            calcul_cout_revient.Parent = panel2;
            calcul_cout_revient.Top = 0;
            calcul_cout_revient.Left = 0;
            calcul_cout_revient.Size = panel2.ClientSize;
            //calcul_cout_revient.Dock = DockStyle.Fill;
            calcul_cout_revient.BringToFront();
            calcul_cout_revient.Show();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu_T menu = new Menu_T();
            menu.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            UserControl_Calculatrice calculatrice = new UserControl_Calculatrice();
            calculatrice.Parent = panel2;
            //ajout_centre.Top = 100;
            //ajout_centre.Left = 150;
            calculatrice.Size = panel2.ClientSize;
            calculatrice.Dock = DockStyle.Fill;
            calculatrice.BringToFront();
            calculatrice.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            sidePanel.Height = button6.Height;
            sidePanel.Top = button6.Top;
            DialogResult reponse = MessageBox.Show("Réinitialisation de l'exercice...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (reponse == DialogResult.Yes)
            {
                //vider les listes
                UserControlAjout_Centre_Analyse.listCentre.Clear();
                UserControlAjout_Centre_Analyse.listCentre_Aux.Clear();
                UserControlCalcul_cout_UO.listUO.Clear();
                UserControlCalcul_cout_UO_Aux.centre_répartition = 0;
                UserControlCalcul_cout_UO_Aux.centre_répartition_aux = 0;
                UserControlCalcul_cout_approv.listAppro.Clear();
                UserControlCalcul_cout_production.listCoutProduction.Clear();
                UserControlCalcul_cout_revient.listCoutRevient.Clear();
                UserControlCalcul_cout_revient.listRésultat.Clear();
                Form_calcul_stock.listStock.Clear();
                UserControl_Calculatrice calculatrice = new UserControl_Calculatrice();

                calculatrice.Parent = panel2;
                //ajout_centre.Top = 100;
                //ajout_centre.Left = 150;
                calculatrice.Size = panel2.ClientSize;
                calculatrice.Dock = DockStyle.Fill;
                calculatrice.BringToFront();
                calculatrice.Show();
            }

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();

        }
    }
}
