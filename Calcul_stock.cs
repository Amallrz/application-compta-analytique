﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class Calcul_stock : UserControl
    {
        public static List<Class_clacul_stock> listStock = new List<Class_clacul_stock>();
        public List<Class_calcul_approv> listAppro = new List<Class_calcul_approv>();
        public Calcul_stock()
        {
            InitializeComponent();
        }

        private void Calcul_stock_Load(object sender, EventArgs e)
        {
            this.listAppro = UserControlCalcul_cout_approv.listAppro;
            //combo
            foreach (Class_calcul_approv obj in listAppro)
            {
                this.metroComboBox1.Items.Add(obj.Matière);
            }
            metroGrid1.Columns.Add("Fiche de Stock", "Fiche de Stock");

            for (int i = 0; i < 4; i++)
            {
                this.metroGrid1.Rows.Add("");
            }
            this.metroGrid1.Rows[0].Cells[0].Value = "Si";
            this.metroGrid1.Rows[1].Cells[0].Value = "E";
            this.metroGrid1.Rows[2].Cells[0].Value = "S";
            this.metroGrid1.Rows[3].Cells[0].Value = "Sf";

            if (listStock.Count != 0)
            {
               for(int i=0;i<listStock.Count;i++)
                {
                    metroGrid1.Columns.Add(listStock.ElementAt(i).Matière, listStock.ElementAt(i).Matière);
                    this.metroGrid1.Rows[0].Cells[listStock.ElementAt(i).Matière].Value = listStock.ElementAt(i).StockInitial;
                    this.metroGrid1.Rows[1].Cells[listStock.ElementAt(i).Matière].Value = listStock.ElementAt(i).Entrée;
                    this.metroGrid1.Rows[2].Cells[listStock.ElementAt(i).Matière].Value = listStock.ElementAt(i).Sortie;
                    this.metroGrid1.Rows[3].Cells[listStock.ElementAt(i).Matière].Value = listStock.ElementAt(i).StockFinal;
                }
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            metroGrid1.Columns.Add(this.metroComboBox1.Text, this.metroComboBox1.Text);
            this.metroGrid1.Rows[0].Cells[this.metroComboBox1.Text].Value = metroTextBox1.Text;
            this.metroGrid1.Rows[1].Cells[this.metroComboBox1.Text].Value = metroTextBox2.Text;
            this.metroGrid1.Rows[2].Cells[this.metroComboBox1.Text].Value = metroTextBox3.Text;
            this.metroGrid1.Rows[3].Cells[this.metroComboBox1.Text].Value = metroTextBox4.Text;
            Class_clacul_stock stock = new Class_clacul_stock();
            stock.Matière = this.metroComboBox1.Text;
            stock.StockInitial = double.Parse(metroTextBox1.Text);
            stock.Entrée = double.Parse(metroTextBox2.Text);
            stock.Sortie = double.Parse(metroTextBox3.Text);
            stock.StockFinal = double.Parse(metroTextBox4.Text);
            listStock.Add(stock);
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {

           
        }
    }
}
