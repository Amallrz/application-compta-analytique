﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class FormHistoriqueCoutComplet : MetroFramework.Forms.MetroForm
    {
        private MySqlConnection connection;
        public FormHistoriqueCoutComplet()
        {
            InitializeComponent();
        }

        private void FormHistoriqueCoutComplet_Load(object sender, EventArgs e)
        {

            metroGrid1.Columns.Add("Nom Produit", "Nom Produit");
            metroGrid1.Columns.Add("Numéro Exercice", "Numéro Exercice");
            metroGrid1.Columns.Add("Date Exercice", "Date Exercice");
            metroGrid1.Columns.Add("Cout Production", "Cout Production");
            metroGrid1.Columns.Add("Cout de Revient", "Cout de Revient");
            metroGrid1.Columns.Add("Résultat", "Résultat");
            this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.metroGrid1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.metroGrid1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.metroGrid1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.metroGrid1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.metroGrid1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            for (int i = 0; i < 1000; i++)
            {
                this.metroGrid1.Rows.Add("");
            }

            connection = new MySqlConnection(Program.ConnectionString);
            try
            {
                connection.Open();
                string query = "SELECT Num_Exercice,Date_Exercice,Cout_production,Cout_revient,Resultat,Produit FROM `coutcomplet`";
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                MySqlDataReader reader = cmd.ExecuteReader();
                int i = 0;
                while (reader.Read()) // If you're expecting only one line, change this to if(reader.Read()).
                {
                    //nom = reader.GetString(1);
                    this.metroGrid1.Rows[i].Cells["Nom Produit"].Value = reader["Produit"].ToString();
                    this.metroGrid1.Rows[i].Cells["Numéro Exercice"].Value = reader["Num_Exercice"].ToString();
                    this.metroGrid1.Rows[i].Cells["Date Exercice"].Value = reader["Date_Exercice"].ToString();
                    this.metroGrid1.Rows[i].Cells["Cout Production"].Value = reader["Cout_production"].ToString();
                    this.metroGrid1.Rows[i].Cells["Cout de Revient"].Value = reader["Cout_revient"].ToString();
                    this.metroGrid1.Rows[i].Cells["Résultat"].Value = reader["Resultat"].ToString();
                    i++;
                }

                reader.Close();
                connection.Close();
                timer1.Start();
                labelheureDate.Text = DateTime.Now.ToLongTimeString();
                labelDate.Text = DateTime.Now.ToLongDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu_T menu = new Menu_T();
            menu.Show();
            this.Hide();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
