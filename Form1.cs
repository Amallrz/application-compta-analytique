﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Compta_Analytique
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        MySqlConnection con;
        bool connecte = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Start();
            labelheureDate.Text = DateTime.Now.ToLongDateString();
            label5.Text = DateTime.Now.ToLongDateString();
        }

        private void buttonAjouter_Click(object sender, EventArgs e)
        {
            //con = new MySqlConnection("database=courpartiel; server=localhost; username=root; password=");
            if (buttonAjouter.Text == "Ajouter")
            {
                //con = new MySqlConnection("database=courpartiel; server=localhost; username=root; password=");
                con = new MySqlConnection(Program.ConnectionString);

                try
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                        //button1.Text = "se deconnecter";
                        connecte = true;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }


            double CA, CV, CS, CF, MCV, MCS, R, RS, TAUX, SR, PM, IS, MS, TM, LO;
            CA = double.Parse(textBoxCA.Text);
            CV = double.Parse(textBoxCV.Text);
            CS = double.Parse(textBoxCS.Text);
            CF = double.Parse(textBoxCF.Text);

            //Calcul du cout sur marge variable
            MCV = CA - CV;

            //Calcul du cout sur marge spécifique
            MCS = CA - CV - CS;

            //Calcul du résultat
            R = MCV - CF;
            RS = MCS - CF;

            //Calcul du taux de marge sur cout variable
            TAUX = ((CA - CV) / CA);

            //Calcul du seuil de rentabilité
            SR = CF / TAUX;

            //Calcul du seuil de rentabilité
            PM = (SR / CA) * 12;

            //Calcul de l'indice de sécurité
            IS = (CA - SR) / CA;

            //Calcul de la marge de sécurité
            MS = CA - SR;

            //Calcul du taux de marge
            TM = MS / CA;

            //Calcul du levier opérationnel
            LO = MCV / R;

            textBoxMCV.Text = MCV.ToString("0.00");
            textBoxMCS.Text = MCS.ToString("0.00");
            textBoxR.Text = R.ToString("0.00");
            textBoxR.Text = RS.ToString("0.00");
            textBoxTaux.Text = TAUX.ToString("0.00");
            textBoxSR.Text = SR.ToString("0.00");
            textBoxPM.Text = PM.ToString("0.00");
            textBoxIS.Text = IS.ToString("0.00");
            textBoxMS.Text = MS.ToString("0.00");
            textBoxTM.Text = TM.ToString("0.00");
            textBoxLO.Text = LO.ToString("0.00");

            //parametre de connection
            //con = new MySqlConnection("database=courpartiel; server=localhost; username=root; password=");
            // @resultat, @mcv, @mcs, @taux, @sr, @tm, @ms, @is, @pm, @lo

            if (connecte == true)
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO partiel(ca, cv, cs, cf,resultat, mcv, mcs, taux, sr, tm, ms,indice,pm,lo,date,heure)" +
                    " VALUES(@ca, @cv, @cs, @cf,@resultat, @mcv, @mcs, @taux, @sr, @tm, @ms, @indice,@pm,@lo,@date,@heure)", con);
                cmd.Parameters.AddWithValue("@ca", Double.Parse(textBoxCA.Text));
                cmd.Parameters.AddWithValue("@cv", Double.Parse(textBoxCV.Text));
                cmd.Parameters.AddWithValue("@cs", Double.Parse(textBoxCS.Text));
                cmd.Parameters.AddWithValue("@cf", Double.Parse(textBoxCF.Text));
                cmd.Parameters.AddWithValue("@resultat", Double.Parse(textBoxR.Text));
                cmd.Parameters.AddWithValue("@mcv", Double.Parse(textBoxMCV.Text));
                cmd.Parameters.AddWithValue("@mcs", Double.Parse(textBoxMCS.Text));
                cmd.Parameters.AddWithValue("@taux", Double.Parse(textBoxTaux.Text));
                cmd.Parameters.AddWithValue("@sr", Double.Parse(textBoxSR.Text));
                cmd.Parameters.AddWithValue("@tm", Double.Parse(textBoxTM.Text));
                cmd.Parameters.AddWithValue("@ms", Double.Parse(textBoxMS.Text));
                cmd.Parameters.AddWithValue("@indice", Double.Parse(textBoxIS.Text));
                cmd.Parameters.AddWithValue("@pm", Double.Parse(textBoxPM.Text));
                cmd.Parameters.AddWithValue("@lo", Double.Parse(textBoxLO.Text));
                cmd.Parameters.AddWithValue("@date", label5.Text);
                cmd.Parameters.AddWithValue("@heure", labelheureDate.Text);

                //pour executer les parametres
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                MessageBox.Show("Bien ajouter");

            }
            else
            {
                MessageBox.Show("vous n'etes pas connecter");
            }
        }

        private void textBoxCV_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxCS_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBoxCA_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCA.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCV_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void textBoxCV_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCV.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCS_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCS.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCF_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == 46 && textBoxCF.Text.IndexOf('.') != -1)
            {
                e.Handled = true;
                return;
            }

            if (!char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
                MessageBox.Show("Entrer une valeur en chiffre");
            }
        }

        private void textBoxCS_Enter(object sender, EventArgs e)
        {
            /*if (textBoxCS.Text == "")
            {
                MessageBox.Show("entrer une valeur");
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //visibilité des indicateurs
            mcs.Visible = true;
            mcv.Visible = true;
            labelresultat.Visible = true;
            labelsr.Visible = true;
            labeltaux.Visible = true;
            labelms.Visible = false;
            labelpm.Visible = false;
            labeltm.Visible = false;
            labello.Visible = false;
            labelis.Visible = false;

            //visibilité des résultats
            textBoxMCV.Visible = true;
            textBoxMCS.Visible = true;
            textBoxR.Visible = true;
            textBoxSR.Visible = true;
            textBoxTaux.Visible = true;
            textBoxMS.Visible = false;
            textBoxPM.Visible = false;
            textBoxTM.Visible = false;
            textBoxLO.Visible = false;
            textBoxIS.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //visibilité des indicateurs
            mcs.Visible = false;
            mcv.Visible = false;
            labelresultat.Visible = false;
            labelsr.Visible = false;
            labeltaux.Visible = false;
            labelms.Visible = true;
            labelpm.Visible = true;
            labeltm.Visible = true;
            labello.Visible = true;
            labelis.Visible = true;

            //visibilité des résultats
            textBoxMCV.Visible = false;
            textBoxMCS.Visible = false;
            textBoxR.Visible = false;
            textBoxSR.Visible = false;
            textBoxTaux.Visible = false;
            textBoxMS.Visible = true;
            textBoxPM.Visible = true;
            textBoxTM.Visible = true;
            textBoxLO.Visible = true;
            textBoxIS.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //visibilité des indicateurs
            mcs.Visible = true;
            mcv.Visible = true;
            labelresultat.Visible = true;
            labelsr.Visible = true;
            labeltaux.Visible = true;
            labelms.Visible = true;
            labelpm.Visible = true;
            labeltm.Visible = true;
            labello.Visible = true;
            labelis.Visible = true;

            //visibilité des résultats
            textBoxMCV.Visible = true;
            textBoxMCS.Visible = true;
            textBoxR.Visible = true;
            textBoxSR.Visible = true;
            textBoxTaux.Visible = true;
            textBoxMS.Visible = true;
            textBoxPM.Visible = true;
            textBoxTM.Visible = true;
            textBoxLO.Visible = true;
            textBoxIS.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu_T menu = new Menu_T();
            menu.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelheureDate.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }
    }
}
