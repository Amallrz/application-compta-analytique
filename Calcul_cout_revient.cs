﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class Calcul_cout_revient : UserControl
    {
        public List<Class_calcul_cout_production> listCoutProduction = new List<Class_calcul_cout_production>();
        public static List<Class_calcul_cout_revient> listCoutRevient = new List<Class_calcul_cout_revient>();
        public static List<Class_résultat_analytique> listRésultat = new List<Class_résultat_analytique>();
        public Calcul_cout_revient()
        {
            InitializeComponent();
        }

        private void Calcul_cout_revient_Load(object sender, EventArgs e)
        {
            if (listCoutRevient.Count != 0)
            {
                metroGrid1.DataSource = null;
                metroGrid1.DataSource = listCoutRevient;
            }
            this.listCoutProduction = Calcul_cout_production.listCoutProduction;
            //remplir combo
            for(int i=0;i<listCoutProduction.Count;i++)
            {
                this.metroComboBox1.Items.Add(listCoutProduction.ElementAt(i).NomProduit);
            }
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //afficher cout de production
            for (int i = 0; i < listCoutProduction.Count; i++)
            {
                if(this.metroComboBox1.Text== listCoutProduction.ElementAt(i).NomProduit)
                {
                    this.metroTextBox2.Text = listCoutProduction.ElementAt(i).Cout_de_production.ToString(); 
                }

                   
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            listCoutRevient.Add(new Class_calcul_cout_revient
            {
                NomProduit = metroComboBox1.Text,
                CoutRevient = double.Parse(this.metroTextBox2.Text)+ double.Parse(this.metroTextBox1.Text)
            });
            metroGrid1.DataSource = null;
            metroGrid1.DataSource = listCoutRevient;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void metroTextBox2_Click(object sender, EventArgs e)
        {

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < listCoutRevient.Count; i++)
            {
                if (listCoutRevient.ElementAt(i).NomProduit == metroComboBox1.Text)
                {
                listRésultat.Add(new Class_résultat_analytique
                {
                    NomProduit = metroComboBox1.Text,
                    Résultat = (double.Parse(this.metroTextBox3.Text) * double.Parse(this.metroTextBox4.Text)) - listCoutRevient.ElementAt(i).CoutRevient
                });
                }
            }
            metroGrid2.DataSource = null;
            metroGrid2.DataSource = listCoutRevient;
        }
    }
}
