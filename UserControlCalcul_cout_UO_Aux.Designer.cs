﻿namespace Application_Compta_Analytique
{
    partial class UserControlCalcul_cout_UO_Aux
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox_quantité = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_nature = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox_répartition = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroTextBox_pourcentage = new MetroFramework.Controls.MetroTextBox();
            this.Label1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            this.Label2 = new MetroFramework.Controls.MetroLabel();
            this.Label3 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton1.Location = new System.Drawing.Point(664, 790);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(157, 46);
            this.metroButton1.TabIndex = 40;
            this.metroButton1.Text = "Calculer";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroTextBox_quantité
            // 
            // 
            // 
            // 
            this.metroTextBox_quantité.CustomButton.Image = null;
            this.metroTextBox_quantité.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox_quantité.CustomButton.Name = "";
            this.metroTextBox_quantité.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox_quantité.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_quantité.CustomButton.TabIndex = 1;
            this.metroTextBox_quantité.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_quantité.CustomButton.UseSelectable = true;
            this.metroTextBox_quantité.CustomButton.Visible = false;
            this.metroTextBox_quantité.Lines = new string[] {
        "0"};
            this.metroTextBox_quantité.Location = new System.Drawing.Point(744, 294);
            this.metroTextBox_quantité.MaxLength = 32767;
            this.metroTextBox_quantité.Name = "metroTextBox_quantité";
            this.metroTextBox_quantité.PasswordChar = '\0';
            this.metroTextBox_quantité.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_quantité.SelectedText = "";
            this.metroTextBox_quantité.SelectionLength = 0;
            this.metroTextBox_quantité.SelectionStart = 0;
            this.metroTextBox_quantité.ShortcutsEnabled = true;
            this.metroTextBox_quantité.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox_quantité.TabIndex = 39;
            this.metroTextBox_quantité.Text = "0";
            this.metroTextBox_quantité.UseSelectable = true;
            this.metroTextBox_quantité.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_quantité.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_nature
            // 
            // 
            // 
            // 
            this.metroTextBox_nature.CustomButton.Image = null;
            this.metroTextBox_nature.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox_nature.CustomButton.Name = "";
            this.metroTextBox_nature.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox_nature.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_nature.CustomButton.TabIndex = 1;
            this.metroTextBox_nature.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_nature.CustomButton.UseSelectable = true;
            this.metroTextBox_nature.CustomButton.Visible = false;
            this.metroTextBox_nature.Lines = new string[] {
        "0"};
            this.metroTextBox_nature.Location = new System.Drawing.Point(744, 244);
            this.metroTextBox_nature.MaxLength = 32767;
            this.metroTextBox_nature.Name = "metroTextBox_nature";
            this.metroTextBox_nature.PasswordChar = '\0';
            this.metroTextBox_nature.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_nature.SelectedText = "";
            this.metroTextBox_nature.SelectionLength = 0;
            this.metroTextBox_nature.SelectionStart = 0;
            this.metroTextBox_nature.ShortcutsEnabled = true;
            this.metroTextBox_nature.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox_nature.TabIndex = 38;
            this.metroTextBox_nature.Text = "0";
            this.metroTextBox_nature.UseSelectable = true;
            this.metroTextBox_nature.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_nature.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox_répartition
            // 
            // 
            // 
            // 
            this.metroTextBox_répartition.CustomButton.Image = null;
            this.metroTextBox_répartition.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox_répartition.CustomButton.Name = "";
            this.metroTextBox_répartition.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox_répartition.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_répartition.CustomButton.TabIndex = 1;
            this.metroTextBox_répartition.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_répartition.CustomButton.UseSelectable = true;
            this.metroTextBox_répartition.CustomButton.Visible = false;
            this.metroTextBox_répartition.Lines = new string[] {
        "0"};
            this.metroTextBox_répartition.Location = new System.Drawing.Point(744, 59);
            this.metroTextBox_répartition.MaxLength = 32767;
            this.metroTextBox_répartition.Name = "metroTextBox_répartition";
            this.metroTextBox_répartition.PasswordChar = '\0';
            this.metroTextBox_répartition.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_répartition.SelectedText = "";
            this.metroTextBox_répartition.SelectionLength = 0;
            this.metroTextBox_répartition.SelectionStart = 0;
            this.metroTextBox_répartition.ShortcutsEnabled = true;
            this.metroTextBox_répartition.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox_répartition.TabIndex = 37;
            this.metroTextBox_répartition.Text = "0";
            this.metroTextBox_répartition.UseSelectable = true;
            this.metroTextBox_répartition.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_répartition.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(498, 294);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(125, 20);
            this.metroLabel1.TabIndex = 36;
            this.metroLabel1.Text = "Quantité de l\'UO";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(498, 244);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(113, 20);
            this.metroLabel5.TabIndex = 35;
            this.metroLabel5.Text = "Nature de l\'UO";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(498, 66);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(151, 20);
            this.metroLabel7.TabIndex = 34;
            this.metroLabel7.Text = "Répartition primaire";
            // 
            // metroButton2
            // 
            this.metroButton2.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.metroButton2.Location = new System.Drawing.Point(664, 352);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(157, 47);
            this.metroButton2.TabIndex = 33;
            this.metroButton2.Text = "Ajouter";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.metroGrid1.ColumnHeadersHeight = 50;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle11;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(119, 410);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.metroGrid1.RowHeadersWidth = 15;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.RowTemplate.Height = 30;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(1164, 355);
            this.metroGrid1.TabIndex = 32;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.metroTextBox_pourcentage);
            this.groupBox1.Controls.Add(this.Label1);
            this.groupBox1.Location = new System.Drawing.Point(516, 126);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(454, 87);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            // 
            // metroTextBox_pourcentage
            // 
            // 
            // 
            // 
            this.metroTextBox_pourcentage.CustomButton.Image = null;
            this.metroTextBox_pourcentage.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox_pourcentage.CustomButton.Name = "";
            this.metroTextBox_pourcentage.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox_pourcentage.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox_pourcentage.CustomButton.TabIndex = 1;
            this.metroTextBox_pourcentage.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox_pourcentage.CustomButton.UseSelectable = true;
            this.metroTextBox_pourcentage.CustomButton.Visible = false;
            this.metroTextBox_pourcentage.Lines = new string[0];
            this.metroTextBox_pourcentage.Location = new System.Drawing.Point(230, 28);
            this.metroTextBox_pourcentage.MaxLength = 32767;
            this.metroTextBox_pourcentage.Name = "metroTextBox_pourcentage";
            this.metroTextBox_pourcentage.PasswordChar = '\0';
            this.metroTextBox_pourcentage.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_pourcentage.SelectedText = "";
            this.metroTextBox_pourcentage.SelectionLength = 0;
            this.metroTextBox_pourcentage.SelectionStart = 0;
            this.metroTextBox_pourcentage.ShortcutsEnabled = true;
            this.metroTextBox_pourcentage.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox_pourcentage.TabIndex = 43;
            this.metroTextBox_pourcentage.UseSelectable = true;
            this.metroTextBox_pourcentage.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox_pourcentage.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Label1.Location = new System.Drawing.Point(22, 32);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(47, 20);
            this.Label1.TabIndex = 44;
            this.Label1.Text = "Label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Label3);
            this.groupBox2.Controls.Add(this.Label2);
            this.groupBox2.Controls.Add(this.metroTextBox1);
            this.groupBox2.Controls.Add(this.metroTextBox2);
            this.groupBox2.Location = new System.Drawing.Point(516, 109);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(454, 120);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.Image = null;
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.Visible = false;
            this.metroTextBox1.Lines = new string[] {
        "0"};
            this.metroTextBox1.Location = new System.Drawing.Point(228, 21);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.ShortcutsEnabled = true;
            this.metroTextBox1.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox1.TabIndex = 47;
            this.metroTextBox1.Text = "0";
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox2
            // 
            // 
            // 
            // 
            this.metroTextBox2.CustomButton.Image = null;
            this.metroTextBox2.CustomButton.Location = new System.Drawing.Point(175, 2);
            this.metroTextBox2.CustomButton.Name = "";
            this.metroTextBox2.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.metroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox2.CustomButton.TabIndex = 1;
            this.metroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox2.CustomButton.UseSelectable = true;
            this.metroTextBox2.CustomButton.Visible = false;
            this.metroTextBox2.Lines = new string[] {
        "0"};
            this.metroTextBox2.Location = new System.Drawing.Point(228, 66);
            this.metroTextBox2.MaxLength = 32767;
            this.metroTextBox2.Name = "metroTextBox2";
            this.metroTextBox2.PasswordChar = '\0';
            this.metroTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2.SelectedText = "";
            this.metroTextBox2.SelectionLength = 0;
            this.metroTextBox2.SelectionStart = 0;
            this.metroTextBox2.ShortcutsEnabled = true;
            this.metroTextBox2.Size = new System.Drawing.Size(203, 30);
            this.metroTextBox2.TabIndex = 46;
            this.metroTextBox2.Text = "0";
            this.metroTextBox2.UseSelectable = true;
            this.metroTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Label2.Location = new System.Drawing.Point(22, 29);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(55, 20);
            this.Label2.TabIndex = 48;
            this.Label2.Text = "Label2";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.Label3.Location = new System.Drawing.Point(22, 69);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(55, 20);
            this.Label3.TabIndex = 49;
            this.Label3.Text = "Label3";
            // 
            // UserControlCalcul_cout_UO_Aux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroTextBox_quantité);
            this.Controls.Add(this.metroTextBox_nature);
            this.Controls.Add(this.metroTextBox_répartition);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroGrid1);
            this.Name = "UserControlCalcul_cout_UO_Aux";
            this.Size = new System.Drawing.Size(1286, 916);
            this.Load += new System.EventHandler(this.UserControlCalcul_cout_UO_Aux_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox metroTextBox_quantité;
        private MetroFramework.Controls.MetroTextBox metroTextBox_nature;
        private MetroFramework.Controls.MetroTextBox metroTextBox_répartition;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTextBox metroTextBox_pourcentage;
        private MetroFramework.Controls.MetroLabel Label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private MetroFramework.Controls.MetroLabel Label2;
        private MetroFramework.Controls.MetroLabel Label3;
    }
}
