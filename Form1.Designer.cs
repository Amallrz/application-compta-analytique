﻿namespace Application_Compta_Analytique
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelheureDate = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAjouter = new System.Windows.Forms.Button();
            this.textBoxLO = new System.Windows.Forms.TextBox();
            this.textBoxPM = new System.Windows.Forms.TextBox();
            this.textBoxMS = new System.Windows.Forms.TextBox();
            this.textBoxTM = new System.Windows.Forms.TextBox();
            this.textBoxIS = new System.Windows.Forms.TextBox();
            this.textBoxTaux = new System.Windows.Forms.TextBox();
            this.textBoxSR = new System.Windows.Forms.TextBox();
            this.textBoxR = new System.Windows.Forms.TextBox();
            this.textBoxMCS = new System.Windows.Forms.TextBox();
            this.textBoxMCV = new System.Windows.Forms.TextBox();
            this.labello = new System.Windows.Forms.Label();
            this.labelpm = new System.Windows.Forms.Label();
            this.labelms = new System.Windows.Forms.Label();
            this.labeltaux = new System.Windows.Forms.Label();
            this.labelis = new System.Windows.Forms.Label();
            this.labeltm = new System.Windows.Forms.Label();
            this.labelsr = new System.Windows.Forms.Label();
            this.labelresultat = new System.Windows.Forms.Label();
            this.mcs = new System.Windows.Forms.Label();
            this.mcv = new System.Windows.Forms.Label();
            this.textBoxCF = new System.Windows.Forms.TextBox();
            this.textBoxCS = new System.Windows.Forms.TextBox();
            this.textBoxCV = new System.Windows.Forms.TextBox();
            this.textBoxCA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1020, 7);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(85, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 32;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(271, 60);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1118, 79);
            this.panel2.TabIndex = 62;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(402, 17);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(220, 28);
            this.label15.TabIndex = 1;
            this.label15.Text = "Calcul coût partiel";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Window;
            this.label5.Location = new System.Drawing.Point(16, 28);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 33;
            this.label5.Text = "label5";
            // 
            // labelheureDate
            // 
            this.labelheureDate.AutoSize = true;
            this.labelheureDate.ForeColor = System.Drawing.SystemColors.Window;
            this.labelheureDate.Location = new System.Drawing.Point(33, 49);
            this.labelheureDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelheureDate.Name = "labelheureDate";
            this.labelheureDate.Size = new System.Drawing.Size(105, 17);
            this.labelheureDate.TabIndex = 32;
            this.labelheureDate.Text = "labelheureDate";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(73, 561);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(85, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button3.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.Location = new System.Drawing.Point(10, 328);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(224, 54);
            this.button3.TabIndex = 5;
            this.button3.Text = "Tous les indicateurs";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.Location = new System.Drawing.Point(10, 408);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(224, 54);
            this.button4.TabIndex = 4;
            this.button4.Text = "Coefficients d\'elasticité";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button2.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(10, 249);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(224, 54);
            this.button2.TabIndex = 2;
            this.button2.Text = "Indicateur de sécurité";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(11, 170);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(224, 54);
            this.button1.TabIndex = 1;
            this.button1.Text = "Indicateur de rentabilité";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.labelheureDate);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 676);
            this.panel1.TabIndex = 61;
            // 
            // buttonAjouter
            // 
            this.buttonAjouter.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonAjouter.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjouter.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAjouter.Location = new System.Drawing.Point(685, 366);
            this.buttonAjouter.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAjouter.Name = "buttonAjouter";
            this.buttonAjouter.Size = new System.Drawing.Size(208, 37);
            this.buttonAjouter.TabIndex = 60;
            this.buttonAjouter.Text = "Ajouter";
            this.buttonAjouter.UseVisualStyleBackColor = false;
            this.buttonAjouter.Click += new System.EventHandler(this.buttonAjouter_Click);
            // 
            // textBoxLO
            // 
            this.textBoxLO.Location = new System.Drawing.Point(1202, 661);
            this.textBoxLO.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLO.Name = "textBoxLO";
            this.textBoxLO.Size = new System.Drawing.Size(132, 22);
            this.textBoxLO.TabIndex = 59;
            this.textBoxLO.Visible = false;
            // 
            // textBoxPM
            // 
            this.textBoxPM.Location = new System.Drawing.Point(1202, 603);
            this.textBoxPM.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPM.Name = "textBoxPM";
            this.textBoxPM.Size = new System.Drawing.Size(132, 22);
            this.textBoxPM.TabIndex = 58;
            this.textBoxPM.Visible = false;
            // 
            // textBoxMS
            // 
            this.textBoxMS.Location = new System.Drawing.Point(1202, 547);
            this.textBoxMS.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMS.Name = "textBoxMS";
            this.textBoxMS.Size = new System.Drawing.Size(132, 22);
            this.textBoxMS.TabIndex = 57;
            this.textBoxMS.Visible = false;
            // 
            // textBoxTM
            // 
            this.textBoxTM.Location = new System.Drawing.Point(1202, 487);
            this.textBoxTM.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTM.Name = "textBoxTM";
            this.textBoxTM.Size = new System.Drawing.Size(132, 22);
            this.textBoxTM.TabIndex = 56;
            this.textBoxTM.Visible = false;
            // 
            // textBoxIS
            // 
            this.textBoxIS.Location = new System.Drawing.Point(1202, 434);
            this.textBoxIS.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxIS.Name = "textBoxIS";
            this.textBoxIS.Size = new System.Drawing.Size(132, 22);
            this.textBoxIS.TabIndex = 55;
            this.textBoxIS.Visible = false;
            // 
            // textBoxTaux
            // 
            this.textBoxTaux.Location = new System.Drawing.Point(656, 658);
            this.textBoxTaux.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTaux.Name = "textBoxTaux";
            this.textBoxTaux.Size = new System.Drawing.Size(132, 22);
            this.textBoxTaux.TabIndex = 54;
            this.textBoxTaux.Visible = false;
            // 
            // textBoxSR
            // 
            this.textBoxSR.Location = new System.Drawing.Point(656, 605);
            this.textBoxSR.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxSR.Name = "textBoxSR";
            this.textBoxSR.Size = new System.Drawing.Size(132, 22);
            this.textBoxSR.TabIndex = 53;
            this.textBoxSR.Visible = false;
            // 
            // textBoxR
            // 
            this.textBoxR.Location = new System.Drawing.Point(656, 551);
            this.textBoxR.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxR.Name = "textBoxR";
            this.textBoxR.Size = new System.Drawing.Size(132, 22);
            this.textBoxR.TabIndex = 52;
            this.textBoxR.Visible = false;
            // 
            // textBoxMCS
            // 
            this.textBoxMCS.Location = new System.Drawing.Point(656, 493);
            this.textBoxMCS.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMCS.Name = "textBoxMCS";
            this.textBoxMCS.Size = new System.Drawing.Size(132, 22);
            this.textBoxMCS.TabIndex = 51;
            this.textBoxMCS.Visible = false;
            // 
            // textBoxMCV
            // 
            this.textBoxMCV.Location = new System.Drawing.Point(656, 437);
            this.textBoxMCV.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMCV.Name = "textBoxMCV";
            this.textBoxMCV.Size = new System.Drawing.Size(132, 22);
            this.textBoxMCV.TabIndex = 50;
            this.textBoxMCV.Visible = false;
            // 
            // labello
            // 
            this.labello.AutoSize = true;
            this.labello.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labello.Location = new System.Drawing.Point(882, 658);
            this.labello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labello.Name = "labello";
            this.labello.Size = new System.Drawing.Size(185, 23);
            this.labello.TabIndex = 49;
            this.labello.Text = "Levier opérationnel";
            this.labello.Visible = false;
            // 
            // labelpm
            // 
            this.labelpm.AutoSize = true;
            this.labelpm.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpm.Location = new System.Drawing.Point(882, 603);
            this.labelpm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelpm.Name = "labelpm";
            this.labelpm.Size = new System.Drawing.Size(105, 23);
            this.labelpm.TabIndex = 48;
            this.labelpm.Text = "Point mort";
            this.labelpm.Visible = false;
            // 
            // labelms
            // 
            this.labelms.AutoSize = true;
            this.labelms.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelms.Location = new System.Drawing.Point(883, 544);
            this.labelms.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelms.Name = "labelms";
            this.labelms.Size = new System.Drawing.Size(168, 23);
            this.labelms.TabIndex = 47;
            this.labelms.Text = "Marge de sécurité";
            this.labelms.Visible = false;
            // 
            // labeltaux
            // 
            this.labeltaux.AutoSize = true;
            this.labeltaux.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeltaux.Location = new System.Drawing.Point(330, 660);
            this.labeltaux.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labeltaux.Name = "labeltaux";
            this.labeltaux.Size = new System.Drawing.Size(297, 23);
            this.labeltaux.TabIndex = 46;
            this.labeltaux.Text = "Taux de marge sur coût variable";
            this.labeltaux.Visible = false;
            // 
            // labelis
            // 
            this.labelis.AutoSize = true;
            this.labelis.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelis.Location = new System.Drawing.Point(882, 434);
            this.labelis.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelis.Name = "labelis";
            this.labelis.Size = new System.Drawing.Size(166, 23);
            this.labelis.TabIndex = 45;
            this.labelis.Text = "Indice de sécurité";
            this.labelis.Visible = false;
            // 
            // labeltm
            // 
            this.labeltm.AutoSize = true;
            this.labeltm.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeltm.Location = new System.Drawing.Point(882, 484);
            this.labeltm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labeltm.Name = "labeltm";
            this.labeltm.Size = new System.Drawing.Size(244, 23);
            this.labeltm.TabIndex = 44;
            this.labeltm.Text = "Taux de marge de sécurité";
            this.labeltm.Visible = false;
            // 
            // labelsr
            // 
            this.labelsr.AutoSize = true;
            this.labelsr.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelsr.Location = new System.Drawing.Point(330, 606);
            this.labelsr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelsr.Name = "labelsr";
            this.labelsr.Size = new System.Drawing.Size(181, 23);
            this.labelsr.TabIndex = 43;
            this.labelsr.Text = "Seuil de rentabilité";
            this.labelsr.Visible = false;
            // 
            // labelresultat
            // 
            this.labelresultat.AutoSize = true;
            this.labelresultat.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelresultat.Location = new System.Drawing.Point(330, 551);
            this.labelresultat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelresultat.Name = "labelresultat";
            this.labelresultat.Size = new System.Drawing.Size(86, 23);
            this.labelresultat.TabIndex = 42;
            this.labelresultat.Text = "Résultat";
            this.labelresultat.Visible = false;
            // 
            // mcs
            // 
            this.mcs.AutoSize = true;
            this.mcs.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcs.Location = new System.Drawing.Point(330, 492);
            this.mcs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mcs.Name = "mcs";
            this.mcs.Size = new System.Drawing.Size(237, 23);
            this.mcs.TabIndex = 41;
            this.mcs.Text = "Marge sur coût spécifique";
            this.mcs.Visible = false;
            // 
            // mcv
            // 
            this.mcv.AutoSize = true;
            this.mcv.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mcv.Location = new System.Drawing.Point(330, 437);
            this.mcv.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mcv.Name = "mcv";
            this.mcv.Size = new System.Drawing.Size(221, 23);
            this.mcv.TabIndex = 40;
            this.mcv.Text = "Marge sur coût variable";
            this.mcv.Visible = false;
            // 
            // textBoxCF
            // 
            this.textBoxCF.Location = new System.Drawing.Point(875, 316);
            this.textBoxCF.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCF.Name = "textBoxCF";
            this.textBoxCF.Size = new System.Drawing.Size(175, 22);
            this.textBoxCF.TabIndex = 39;
            this.textBoxCF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCF_KeyPress);
            // 
            // textBoxCS
            // 
            this.textBoxCS.Location = new System.Drawing.Point(876, 267);
            this.textBoxCS.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCS.Name = "textBoxCS";
            this.textBoxCS.Size = new System.Drawing.Size(175, 22);
            this.textBoxCS.TabIndex = 38;
            this.textBoxCS.TextChanged += new System.EventHandler(this.textBoxCS_TextChanged);
            this.textBoxCS.Enter += new System.EventHandler(this.textBoxCS_Enter);
            this.textBoxCS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCS_KeyPress);
            // 
            // textBoxCV
            // 
            this.textBoxCV.Location = new System.Drawing.Point(876, 212);
            this.textBoxCV.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCV.Name = "textBoxCV";
            this.textBoxCV.Size = new System.Drawing.Size(175, 22);
            this.textBoxCV.TabIndex = 37;
            this.textBoxCV.TextChanged += new System.EventHandler(this.textBoxCV_TextChanged);
            this.textBoxCV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCV_KeyPress_1);
            // 
            // textBoxCA
            // 
            this.textBoxCA.Location = new System.Drawing.Point(875, 159);
            this.textBoxCA.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCA.Name = "textBoxCA";
            this.textBoxCA.Size = new System.Drawing.Size(176, 22);
            this.textBoxCA.TabIndex = 36;
            this.textBoxCA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCA_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(547, 316);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 23);
            this.label4.TabIndex = 35;
            this.label4.Text = "Charge fixe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(548, 265);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 23);
            this.label3.TabIndex = 34;
            this.label3.Text = "Charge fixe spécifique";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(547, 209);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 23);
            this.label2.TabIndex = 33;
            this.label2.Text = "Charge variable";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(548, 159);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 23);
            this.label1.TabIndex = 32;
            this.label1.Text = "Chiffre d\'affaires";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 756);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonAjouter);
            this.Controls.Add(this.textBoxLO);
            this.Controls.Add(this.textBoxPM);
            this.Controls.Add(this.textBoxMS);
            this.Controls.Add(this.textBoxTM);
            this.Controls.Add(this.textBoxIS);
            this.Controls.Add(this.textBoxTaux);
            this.Controls.Add(this.textBoxSR);
            this.Controls.Add(this.textBoxR);
            this.Controls.Add(this.textBoxMCS);
            this.Controls.Add(this.textBoxMCV);
            this.Controls.Add(this.labello);
            this.Controls.Add(this.labelpm);
            this.Controls.Add(this.labelms);
            this.Controls.Add(this.labeltaux);
            this.Controls.Add(this.labelis);
            this.Controls.Add(this.labeltm);
            this.Controls.Add(this.labelsr);
            this.Controls.Add(this.labelresultat);
            this.Controls.Add(this.mcs);
            this.Controls.Add(this.mcv);
            this.Controls.Add(this.textBoxCF);
            this.Controls.Add(this.textBoxCS);
            this.Controls.Add(this.textBoxCV);
            this.Controls.Add(this.textBoxCA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelheureDate;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonAjouter;
        private System.Windows.Forms.TextBox textBoxLO;
        private System.Windows.Forms.TextBox textBoxPM;
        private System.Windows.Forms.TextBox textBoxMS;
        private System.Windows.Forms.TextBox textBoxTM;
        private System.Windows.Forms.TextBox textBoxIS;
        private System.Windows.Forms.TextBox textBoxTaux;
        private System.Windows.Forms.TextBox textBoxSR;
        private System.Windows.Forms.TextBox textBoxR;
        private System.Windows.Forms.TextBox textBoxMCS;
        private System.Windows.Forms.TextBox textBoxMCV;
        private System.Windows.Forms.Label labello;
        private System.Windows.Forms.Label labelpm;
        private System.Windows.Forms.Label labelms;
        private System.Windows.Forms.Label labeltaux;
        private System.Windows.Forms.Label labelis;
        private System.Windows.Forms.Label labeltm;
        private System.Windows.Forms.Label labelsr;
        private System.Windows.Forms.Label labelresultat;
        private System.Windows.Forms.Label mcs;
        private System.Windows.Forms.Label mcv;
        private System.Windows.Forms.TextBox textBoxCF;
        private System.Windows.Forms.TextBox textBoxCS;
        private System.Windows.Forms.TextBox textBoxCV;
        private System.Windows.Forms.TextBox textBoxCA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}