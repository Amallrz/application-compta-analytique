﻿namespace Application_Compta_Analytique
{
    partial class UserControl_Calculatrice
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinEditors.UltraWinCalc.CalculatorButton calculatorButton1 = new Infragistics.Win.UltraWinEditors.UltraWinCalc.CalculatorButton(15);
            this.ultraCalculator1 = new Infragistics.Win.UltraWinEditors.UltraWinCalc.UltraCalculator();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalculator1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraCalculator1
            // 
            calculatorButton1.Key = ".";
            calculatorButton1.KeyCodeAlternateValue = 190;
            calculatorButton1.KeyCodeValue = 110;
            calculatorButton1.Text = ",";
            this.ultraCalculator1.Buttons.AddRange(new Infragistics.Win.UltraWinEditors.UltraWinCalc.CalculatorButton[] {
            calculatorButton1});
            this.ultraCalculator1.Location = new System.Drawing.Point(470, 196);
            this.ultraCalculator1.Name = "ultraCalculator1";
            this.ultraCalculator1.Size = new System.Drawing.Size(435, 490);
            this.ultraCalculator1.TabIndex = 0;
            this.ultraCalculator1.Text = "0,";
            this.ultraCalculator1.AfterCalculationComplete += new System.EventHandler(this.ultraCalculator1_AfterCalculationComplete);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(631, 150);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(114, 25);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Calculatrice";
            // 
            // UserControl_Calculatrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.ultraCalculator1);
            this.Name = "UserControl_Calculatrice";
            this.Size = new System.Drawing.Size(1194, 673);
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalculator1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraWinCalc.UltraCalculator ultraCalculator1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}
