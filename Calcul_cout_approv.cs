﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class Calcul_cout_approv : MetroFramework.Forms.MetroForm
    {
        public List<Class_des_UO> listUO = new List<Class_des_UO>();
        public static List<Class_calcul_approv> listAppro = new List<Class_calcul_approv>();
        public Calcul_cout_approv()
        {
            InitializeComponent();
            this.listUO = Calcul_cout_UO.listUO;
        }

        private void Calcul_cout_approv_Load(object sender, EventArgs e)
        {
            if(listAppro.Count!=0)
            {
                metroGrid1.DataSource = null;
                metroGrid1.DataSource = listAppro;
            }

        }

        private void ajouterButton_Click(object sender, EventArgs e)
        {
            string matière = this.metroTextBox2.Text;
            double prix=0;
            double prixUnit = double.Parse(this.metroTextBox4.Text);
            int quantité = int.Parse(this.metroTextBox3.Text);
            double autreCout = double.Parse(this.metroTextBox5.Text);
            prix = quantité * prixUnit;
            listAppro.Add(new Class_calcul_approv
            {
                Matière = matière,
                Quantité= quantité,
                Cout =prix+ (prix * this.listUO.ElementAt(0).getCout()) / this.listUO.ElementAt(0).getNature()+ autreCout
            });
            /*listAppro.Add(new Class_calcul_approv
            {
                Matière = "mp1",
                Cout = 100
            });
            listAppro.Add(new Class_calcul_approv
            {
                Matière = "mp2",
                Cout = 200
            });*/
            metroGrid1.DataSource = null;
            metroGrid1.DataSource = listAppro;
            /*metroGrid1.Columns.Add(matière,matière);
            this.metroGrid1.Rows[0].Cells[matière].Value = "";
            this.metroGrid1.Rows[0].Cells[matière].Value = 
                (prix * this.listUO.ElementAt(0).getCout())/ this.listUO.ElementAt(0).getNature();*/

        }

        private void metroRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
          
        }


        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            Calcul_cout_production form = new Calcul_cout_production();
            form.Show();
            this.Hide();
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            Calcul_cout_UO form = new Calcul_cout_UO();
            form.Show();
            this.Hide();
        }
    }
}
