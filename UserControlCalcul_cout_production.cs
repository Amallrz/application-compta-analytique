﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application_Compta_Analytique.Classes;

namespace Application_Compta_Analytique
{
    public partial class UserControlCalcul_cout_production : UserControl
    {
        public List<Class_calcul_approv> listAppro = new List<Class_calcul_approv>();
        public static List<Class_calcul_cout_production> listCoutProduction = new List<Class_calcul_cout_production>();
        public List<Class_clacul_stock> listStock = new List<Class_clacul_stock>();
        public UserControlCalcul_cout_production()
        {
            InitializeComponent();
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            metroGrid1.Columns.Add(this.metroTextBox1.Text, this.metroTextBox1.Text);
        }

        private void UserControlCalcul_cout_production_Load(object sender, EventArgs e)
        {
            this.listStock = Form_calcul_stock.listStock;
            //remplir combobox
            this.listAppro = UserControlCalcul_cout_approv.listAppro;
            foreach (Class_calcul_approv obj in listAppro)
            {
                this.metroComboBox1.Items.Add(obj.Matière);
            }
            this.metroComboBox1.Items.Add("MOD de production");
            this.metroComboBox1.Items.Add("Charges indirectes");
            metroRadioButton2.Checked = true;
            //metro grid
            remplir_Grid();

            if (listCoutProduction.Count != 0)
            {
                /* MessageBox.Show("list est remplie i= "+ listCoutProduction.Count+" j="+ listCoutProduction.ElementAt(0).Charge.Count);
                 for (int i = 0; i < listCoutProduction.Count; i++)
                 {
                     MessageBox.Show("nom produit "+listCoutProduction.ElementAt(i).NomProduit.ToString());
                     for (int j = 0; j < listCoutProduction.ElementAt(i).Charge.Count; j++)
                     {
                         MessageBox.Show( "charge "+listCoutProduction.ElementAt(i).Charge.ElementAt(j).ToString());
                     }
                     MessageBox.Show("cout produit " + listCoutProduction.ElementAt(i).Cout_de_production);
                 }*/
                //remplir grid par la liste
                for (int i = 0; i < listCoutProduction.Count; i++)
                {
                    metroGrid1.Columns.Add(listCoutProduction.ElementAt(i).NomProduit, listCoutProduction.ElementAt(i).NomProduit);
                    this.metroGrid1.Columns[listCoutProduction.ElementAt(i).NomProduit].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    for (int j = 0; j < listCoutProduction.ElementAt(i).Charge.Count; j++)
                    {
                        //les charges sont deja ordonné dans la liste
                        this.metroGrid1.Rows[j].Cells[i + 1].Value = listCoutProduction.ElementAt(i).Charge.ElementAt(j);
                    }
                    this.metroGrid1.Rows[listCoutProduction.ElementAt(i).Charge.Count].Cells[i + 1].Value = listCoutProduction.ElementAt(i).Cout_de_production;
                }
            }

        }
        public void remplir_Grid()
        {
            metroGrid1.Columns.Add("Charges directes et indirectes", "Charges directes et indirectes");
            this.metroGrid1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            for (int i = 0; i < this.metroComboBox1.Items.Count; i++)
            {
                this.metroGrid1.Rows.Add("");
                this.metroGrid1.Rows[i].Cells[0].Value = this.metroComboBox1.Items[i];
            }
            this.metroGrid1.Rows[this.metroComboBox1.Items.Count].Cells[0].Value = "Coût de production";

        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listStock.Count == 0)
            {
                this.metroTextBox5.Enabled = true;
                //calcul du cu matiere premiere
                for (int i = 0; i < listAppro.Count; i++)
                {
                    if (metroComboBox1.Text == listAppro.ElementAt(i).Matière)
                    {
                        this.metroTextBox5.Enabled = false;
                        this.metroTextBox5.Text = (listAppro.ElementAt(i).Cout / listAppro.ElementAt(i).Quantité).ToString();
                    }
                }
            }
            else
            {
                metroRadioButton1.Checked = true;
                for (int i = 0; i < listStock.Count; i++)
                {
                    if (metroComboBox1.Text == listStock.ElementAt(i).Matière)
                    {                      
                        this.metroTextBox3.Text = listStock.ElementAt(i).Sortie.ToString();
                    }
                }
            }
        }

        private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (metroRadioButton1.Checked == true)
            {
                this.groupBox4.Visible = false;
                this.groupBox3.Visible = true;
            }
        }

        private void metroRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (metroRadioButton2.Checked == true)
            {
                this.groupBox3.Visible = false;
                this.groupBox4.Visible = true;
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < this.metroGrid1.ColumnCount; i++)
            {
                double cout = 0;
                for (int j = 0; j < this.metroComboBox1.Items.Count; j++)
                {
                    cout += double.Parse(this.metroGrid1.Rows[j].Cells[i].Value.ToString());
                }
                this.metroGrid1.Rows[this.metroComboBox1.Items.Count].Cells[i].Value = cout;
            }
            //MessageBox.Show("Column count" + metroGrid1.ColumnCount);
            //remplir list et faire le binding avec grid
            //list des matières premières
            Class_calcul_cout_production class_calcul_production;
            /*for(int i=0; i <Calcul_cout_approv.listAppro.Count;i++)
            {
                class_calcul_production.listAppro.Add(Calcul_cout_approv.listAppro[i].);
            }*/
            //list 
            List<double> listCharges;
            for (int i = 1; i < metroGrid1.ColumnCount; i++)
            {
                listCharges = new List<double>();
                class_calcul_production = new Class_calcul_cout_production();
                class_calcul_production.NomProduit = metroGrid1.Columns[i].Name.ToString();
                for (int j = 0; j < metroGrid1.RowCount - 1; j++)
                {
                    listCharges.Add(double.Parse(metroGrid1.Rows[j].Cells[i].Value.ToString()));
                }
                class_calcul_production.Charge = listCharges;
                class_calcul_production.Cout_de_production = double.Parse(metroGrid1.Rows[metroGrid1.RowCount - 1].Cells[i].Value.ToString());
                listCoutProduction.Add(class_calcul_production);
            }

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            double coutUo = 0;
            double quantité = 0;
            double montant = 0;
            if (metroRadioButton1.Checked)
            {
                montant = double.Parse(metroTextBox3.Text);
            }
            if (metroRadioButton2.Checked)
            {
                quantité = double.Parse(this.metroTextBox4.Text);
                coutUo = double.Parse(this.metroTextBox5.Text);
                montant = quantité * coutUo;
            }
            string charge = metroComboBox1.SelectedItem.ToString();
            for (int i = 0; i < this.metroComboBox1.Items.Count; i++)
            {
                if (charge == this.metroGrid1.Rows[i].Cells[0].Value.ToString())
                {
                    this.metroGrid1.Rows[i].Cells[metroTextBox1.Text].Value = montant;
                }

            }
            this.metroTextBox4.Text = "0";
            this.metroTextBox5.Text = "0";
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
