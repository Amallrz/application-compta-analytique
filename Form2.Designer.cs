﻿namespace Application_Compta_Analytique
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.labelheureDate = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxCECAR = new System.Windows.Forms.TextBox();
            this.textBoxCEPD = new System.Windows.Forms.TextBox();
            this.textBoxRVariation = new System.Windows.Forms.TextBox();
            this.textBoxRInitial = new System.Windows.Forms.TextBox();
            this.textBoxCAVariation = new System.Windows.Forms.TextBox();
            this.textBoxCAInitial = new System.Windows.Forms.TextBox();
            this.textBoxDemandeVariation = new System.Windows.Forms.TextBox();
            this.textBoxDemandeInitial = new System.Windows.Forms.TextBox();
            this.textBoxPrixVariation = new System.Windows.Forms.TextBox();
            this.textBoxPrixInitial = new System.Windows.Forms.TextBox();
            this.buttonAjouter = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(267, 60);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(949, 89);
            this.panel2.TabIndex = 53;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(850, 16);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(85, 62);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 33;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(292, 29);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(220, 28);
            this.label15.TabIndex = 1;
            this.label15.Text = "Calcul coût partiel";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelheureDate
            // 
            this.labelheureDate.AutoSize = true;
            this.labelheureDate.ForeColor = System.Drawing.SystemColors.Window;
            this.labelheureDate.Location = new System.Drawing.Point(54, 39);
            this.labelheureDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelheureDate.Name = "labelheureDate";
            this.labelheureDate.Size = new System.Drawing.Size(46, 17);
            this.labelheureDate.TabIndex = 34;
            this.labelheureDate.Text = "label5";
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.ForeColor = System.Drawing.SystemColors.Window;
            this.labelDate.Location = new System.Drawing.Point(31, 16);
            this.labelDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(46, 17);
            this.labelDate.TabIndex = 35;
            this.labelDate.Text = "label5";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.Location = new System.Drawing.Point(4, 387);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(224, 54);
            this.button4.TabIndex = 3;
            this.button4.Text = "Coefficients d\'elasticité";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button3.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.Location = new System.Drawing.Point(4, 310);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(224, 54);
            this.button3.TabIndex = 2;
            this.button3.Text = "Tous les indicateurs";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button2.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(4, 233);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(224, 54);
            this.button2.TabIndex = 1;
            this.button2.Text = "Indicateur de sécurité";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(5, 155);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(224, 54);
            this.button1.TabIndex = 0;
            this.button1.Text = "Indicateur de rentabilité";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.labelheureDate);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.labelDate);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(20, 60);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(247, 687);
            this.panel1.TabIndex = 52;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(74, 602);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(85, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // textBoxCECAR
            // 
            this.textBoxCECAR.Location = new System.Drawing.Point(887, 596);
            this.textBoxCECAR.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCECAR.Name = "textBoxCECAR";
            this.textBoxCECAR.Size = new System.Drawing.Size(147, 22);
            this.textBoxCECAR.TabIndex = 51;
            // 
            // textBoxCEPD
            // 
            this.textBoxCEPD.Location = new System.Drawing.Point(887, 511);
            this.textBoxCEPD.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCEPD.Name = "textBoxCEPD";
            this.textBoxCEPD.Size = new System.Drawing.Size(147, 22);
            this.textBoxCEPD.TabIndex = 50;
            // 
            // textBoxRVariation
            // 
            this.textBoxRVariation.Location = new System.Drawing.Point(1040, 357);
            this.textBoxRVariation.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxRVariation.Name = "textBoxRVariation";
            this.textBoxRVariation.Size = new System.Drawing.Size(132, 22);
            this.textBoxRVariation.TabIndex = 49;
            this.textBoxRVariation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxRVariation_KeyPress);
            // 
            // textBoxRInitial
            // 
            this.textBoxRInitial.Location = new System.Drawing.Point(1040, 290);
            this.textBoxRInitial.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxRInitial.Name = "textBoxRInitial";
            this.textBoxRInitial.Size = new System.Drawing.Size(132, 22);
            this.textBoxRInitial.TabIndex = 48;
            this.textBoxRInitial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxRInitial_KeyPress);
            // 
            // textBoxCAVariation
            // 
            this.textBoxCAVariation.Location = new System.Drawing.Point(852, 357);
            this.textBoxCAVariation.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCAVariation.Name = "textBoxCAVariation";
            this.textBoxCAVariation.Size = new System.Drawing.Size(132, 22);
            this.textBoxCAVariation.TabIndex = 47;
            this.textBoxCAVariation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCAVariation_KeyPress);
            // 
            // textBoxCAInitial
            // 
            this.textBoxCAInitial.Location = new System.Drawing.Point(852, 291);
            this.textBoxCAInitial.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCAInitial.Name = "textBoxCAInitial";
            this.textBoxCAInitial.Size = new System.Drawing.Size(132, 22);
            this.textBoxCAInitial.TabIndex = 46;
            this.textBoxCAInitial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCAInitial_KeyPress);
            // 
            // textBoxDemandeVariation
            // 
            this.textBoxDemandeVariation.Location = new System.Drawing.Point(647, 357);
            this.textBoxDemandeVariation.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDemandeVariation.Name = "textBoxDemandeVariation";
            this.textBoxDemandeVariation.Size = new System.Drawing.Size(132, 22);
            this.textBoxDemandeVariation.TabIndex = 45;
            this.textBoxDemandeVariation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxDemandeVariation_KeyPress);
            // 
            // textBoxDemandeInitial
            // 
            this.textBoxDemandeInitial.Location = new System.Drawing.Point(647, 293);
            this.textBoxDemandeInitial.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDemandeInitial.Name = "textBoxDemandeInitial";
            this.textBoxDemandeInitial.Size = new System.Drawing.Size(132, 22);
            this.textBoxDemandeInitial.TabIndex = 44;
            this.textBoxDemandeInitial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxDemandeInitial_KeyPress);
            // 
            // textBoxPrixVariation
            // 
            this.textBoxPrixVariation.Location = new System.Drawing.Point(453, 357);
            this.textBoxPrixVariation.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPrixVariation.Name = "textBoxPrixVariation";
            this.textBoxPrixVariation.Size = new System.Drawing.Size(132, 22);
            this.textBoxPrixVariation.TabIndex = 43;
            this.textBoxPrixVariation.TextChanged += new System.EventHandler(this.textBoxPrixVariation_TextChanged);
            this.textBoxPrixVariation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrixVariation_KeyPress);
            // 
            // textBoxPrixInitial
            // 
            this.textBoxPrixInitial.Location = new System.Drawing.Point(453, 293);
            this.textBoxPrixInitial.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPrixInitial.Name = "textBoxPrixInitial";
            this.textBoxPrixInitial.Size = new System.Drawing.Size(132, 22);
            this.textBoxPrixInitial.TabIndex = 42;
            this.textBoxPrixInitial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrixInitial_KeyPress);
            // 
            // buttonAjouter
            // 
            this.buttonAjouter.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonAjouter.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjouter.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonAjouter.Location = new System.Drawing.Point(647, 418);
            this.buttonAjouter.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAjouter.Name = "buttonAjouter";
            this.buttonAjouter.Size = new System.Drawing.Size(199, 47);
            this.buttonAjouter.TabIndex = 41;
            this.buttonAjouter.Text = "Ajouter";
            this.buttonAjouter.UseVisualStyleBackColor = false;
            this.buttonAjouter.Click += new System.EventHandler(this.buttonAjouter_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(449, 595);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(319, 23);
            this.label8.TabIndex = 40;
            this.label8.Text = "Coefficient d\'élasticité CA/Résultat";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(449, 511);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(338, 23);
            this.label7.TabIndex = 39;
            this.label7.Text = "Coefficient d\'élasticité Prix/Demande";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(342, 293);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 17);
            this.label6.TabIndex = 38;
            this.label6.Text = "Variation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(342, 357);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 37;
            this.label5.Text = "Initial";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1057, 223);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 23);
            this.label4.TabIndex = 36;
            this.label4.Text = "Résultat";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(838, 223);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 23);
            this.label3.TabIndex = 35;
            this.label3.Text = "Chiffre d\'affaires";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(658, 223);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 23);
            this.label2.TabIndex = 34;
            this.label2.Text = "Demande";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(487, 223);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 23);
            this.label1.TabIndex = 33;
            this.label1.Text = "Prix";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 767);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textBoxCECAR);
            this.Controls.Add(this.textBoxCEPD);
            this.Controls.Add(this.textBoxRVariation);
            this.Controls.Add(this.textBoxRInitial);
            this.Controls.Add(this.textBoxCAVariation);
            this.Controls.Add(this.textBoxCAInitial);
            this.Controls.Add(this.textBoxDemandeVariation);
            this.Controls.Add(this.textBoxDemandeInitial);
            this.Controls.Add(this.textBoxPrixVariation);
            this.Controls.Add(this.textBoxPrixInitial);
            this.Controls.Add(this.buttonAjouter);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelheureDate;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxCECAR;
        private System.Windows.Forms.TextBox textBoxCEPD;
        private System.Windows.Forms.TextBox textBoxRVariation;
        private System.Windows.Forms.TextBox textBoxRInitial;
        private System.Windows.Forms.TextBox textBoxCAVariation;
        private System.Windows.Forms.TextBox textBoxCAInitial;
        private System.Windows.Forms.TextBox textBoxDemandeVariation;
        private System.Windows.Forms.TextBox textBoxDemandeInitial;
        private System.Windows.Forms.TextBox textBoxPrixVariation;
        private System.Windows.Forms.TextBox textBoxPrixInitial;
        private System.Windows.Forms.Button buttonAjouter;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}